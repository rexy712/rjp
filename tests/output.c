#include "rjp_internal.h"
#include "rjp_value.h"

#include <stdio.h>
#include <string.h>

typedef struct{
	RJP_value* (*create)(void);
	const char* res;
	int fmt;
}test_pair;

//Basic values excluding float because precision bs and stuff
RJP_value* case_1(void){
	return rjp_new_null();
}
RJP_value* case_2(void){
	return rjp_new_int(5);
}
RJP_value* case_3(void){
	return rjp_new_bool(1);
}
RJP_value* case_4(void){
	return rjp_new_bool(0);
}
RJP_value* case_5(void){
	return rjp_new_object();
}
RJP_value* case_6(void){
	return rjp_new_array();
}
RJP_value* case_7(void){
	return rjp_new_ordered_object();
}
//handle object with member
RJP_value* case_8(void){
	RJP_value* obj = rjp_new_object();
	rjp_add_member(obj, "key", 0, rjp_new_int(7));
	return obj;
}
//handle object with subobject
RJP_value* case_9(void){
	RJP_value* obj = rjp_new_object();
	RJP_value* sub = rjp_new_member(obj, "key", 0);
	rjp_set_object(sub);
	rjp_add_member(sub, "subkey", 0, rjp_new_bool(0));
	return obj;
}
//handle object with multiple members
RJP_value* case_10(void){
	RJP_value* obj = rjp_new_object();
	RJP_value* sub = rjp_new_member(obj, "key", 0);
	rjp_set_object(sub);
	rjp_add_member(sub, "subkey", 0, rjp_new_bool(0));
	rjp_add_member(sub, "subkey2", 0, rjp_new_bool(1));
	return obj;
}
//handle object member ordering
RJP_value* case_11(void){
	RJP_value* obj = rjp_new_object();
	RJP_value* sub = rjp_new_member(obj, "key", 0);
	rjp_set_object(sub);
	rjp_add_member(sub, "subkey2", 0, rjp_new_bool(1));
	rjp_add_member(sub, "subkey", 0, rjp_new_bool(0));
	return obj;
}
//handle orderedobject member ordering
RJP_value* case_12(void){
	RJP_value* obj = rjp_new_object();
	RJP_value* sub = rjp_new_member(obj, "key", 0);
	rjp_set_ordered_object(sub);
	rjp_add_member(sub, "subkey", 0, rjp_new_bool(0));
	rjp_add_member(sub, "subkey2", 0, rjp_new_bool(1));
	return obj;
}
//handle orderedobject member ordering pt2
RJP_value* case_13(void){
	RJP_value* obj = rjp_new_object();
	RJP_value* sub = rjp_new_member(obj, "key", 0);
	rjp_set_ordered_object(sub);
	rjp_add_member(sub, "subkey2", 0, rjp_new_bool(1));
	rjp_add_member(sub, "subkey", 0, rjp_new_bool(0));
	return obj;
}
//handle array with element
RJP_value* case_14(void){
	RJP_value* arr = rjp_new_array();
	rjp_add_element(arr, rjp_new_int(5));
	return arr;
}
//handle array with subarray
RJP_value* case_15(void){
	RJP_value* arr = rjp_new_array();
	RJP_value* sub = rjp_new_element(arr);
	rjp_set_array(sub);
	rjp_add_element(sub, rjp_new_bool(0));
	return arr;
}
//handle array with multiple elements
RJP_value* case_16(void){
	RJP_value* arr = rjp_new_array();
	RJP_value* sub = rjp_new_element(arr);
	rjp_set_array(sub);
	rjp_add_element(sub, rjp_new_bool(0));
	rjp_add_element(sub, rjp_new_bool(1));
	return arr;
}
//handle array with multiple elements and subarray
RJP_value* case_17(void){
	RJP_value* arr = rjp_new_array();
	rjp_add_element(arr, rjp_new_int(5));
	RJP_value* sub = rjp_new_element(arr);
	rjp_set_array(sub);
	rjp_add_element(sub, rjp_new_bool(0));
	rjp_add_element(sub, rjp_new_bool(1));
	return arr;
}
//handle array with subobject with subarray
RJP_value* case_18(void){
	RJP_value* arr = rjp_new_array();
	rjp_add_element(arr, rjp_new_int(5));
	RJP_value* subobj = rjp_new_element(arr);
	rjp_set_object(subobj);
	RJP_value* subarr = rjp_new_member(subobj, "key", 0);
	rjp_set_array(subarr);
	rjp_add_element(subarr, rjp_new_bool(0));
	return arr;
}
//handle object with many members
RJP_value* case_19(void){
	char c[] = "key0";
	RJP_value* arr = rjp_new_array();
	rjp_add_element(arr, rjp_new_int(5));
	RJP_value* subobj = rjp_new_element(arr);
	rjp_set_object(subobj);
	for(int i = 0;i < 10;++i){
		RJP_value* newmem = rjp_new_member(subobj, c, 0);
		c[3] += 1;
		if(i % 2 == 0)
			rjp_set_bool(newmem, 1);
		else
			rjp_set_bool(newmem, 0);
	}
	return arr;
}
//handle orderedobject with many members as array element
RJP_value* case_20(void){
	char c[] = "key9";
	RJP_value* arr = rjp_new_array();
	rjp_add_element(arr, rjp_new_int(5));
	RJP_value* subobj = rjp_new_element(arr);
	rjp_set_ordered_object(subobj);
	for(int i = 0;i < 10;++i){
		RJP_value* newmem = rjp_new_member(subobj, c, 0);
		c[3] -= 1;
		if(i % 2 == 0)
			rjp_set_bool(newmem, 1);
		else
			rjp_set_bool(newmem, 0);
	}
	return arr;
}
//handle array with many element as object member
RJP_value* case_21(void){
	RJP_value* obj = rjp_new_object();
	RJP_value* arr = rjp_new_member(obj, "arr", 0);
	rjp_set_array(arr);
	for(int i = 0;i < 10;++i)
		rjp_set_int(rjp_new_element(arr), i);
	return obj;
}
//handle unorderedobject conversion
RJP_value* case_22(void){
	RJP_value* obj = rjp_new_ordered_object();
	RJP_value* sub = rjp_new_member(obj, "key", 0);
	rjp_set_ordered_object(sub);
	rjp_add_member(sub, "subkey", 0, rjp_new_bool(0));
	rjp_add_member(sub, "subkey2", 0, rjp_new_bool(1));
	rjp_object_to_unordered(sub);
	return obj;
}
//handle unorderedobject conversion
RJP_value* case_23(void){
	RJP_value* obj = rjp_new_ordered_object();
	RJP_value* sub = rjp_new_member(obj, "key", 0);
	rjp_set_ordered_object(sub);
	rjp_add_member(sub, "subkey2", 0, rjp_new_bool(1));
	rjp_add_member(sub, "subkey", 0, rjp_new_bool(0));
	rjp_object_to_unordered(sub);
	return obj;
}
//handle orderedobject conversion
RJP_value* case_24(void){
	RJP_value* obj = rjp_new_ordered_object();
	RJP_value* sub = rjp_new_member(obj, "key", 0);
	rjp_set_object(sub);
	rjp_add_member(sub, "subkey", 0, rjp_new_bool(0));
	rjp_add_member(sub, "subkey2", 0, rjp_new_bool(1));
	rjp_object_to_ordered(sub);
	return obj;
}
//handle orderedobject conversion
RJP_value* case_25(void){
	RJP_value* obj = rjp_new_ordered_object();
	RJP_value* sub = rjp_new_member(obj, "key", 0);
	rjp_set_object(sub);
	rjp_add_member(sub, "subkey2", 0, rjp_new_bool(1));
	rjp_object_to_ordered(sub);
	rjp_add_member(sub, "subkey", 0, rjp_new_bool(0));
	return obj;
}
RJP_value* case_26(void){
	return rjp_new_string("string", 0);
}
RJP_value* case_27(void){
	return rjp_new_string("", 0);
}
RJP_value* case_28(void){
	RJP_value* obj = rjp_new_object();
	RJP_value* str = rjp_new_member(obj, "key", 0);
	rjp_set_string(str, "string", 0);
	return obj;
}
RJP_value* case_29(void){
	RJP_value* obj = rjp_new_object();
	RJP_value* str = rjp_new_member(obj, "key", 0);
	rjp_set_string(str, "", 0);
	return obj;
}

static test_pair tests[] = {
	{case_1, "null", RJP_FORMAT_NONE},
	{case_2, "5", RJP_FORMAT_NONE},
	{case_3, "true", RJP_FORMAT_NONE},
	{case_4, "false", RJP_FORMAT_NONE},
	{case_5, "{}", RJP_FORMAT_NONE},
	{case_6, "[]", RJP_FORMAT_NONE},
	{case_7, "{}", RJP_FORMAT_NONE},
	{case_8, "{\"key\":7}", RJP_FORMAT_NONE},
	{case_9, "{\"key\":{\"subkey\":false}}", RJP_FORMAT_NONE},
	{case_10, "{\"key\":{\"subkey\":false,\"subkey2\":true}}", RJP_FORMAT_NONE},
	{case_11, "{\"key\":{\"subkey\":false,\"subkey2\":true}}", RJP_FORMAT_NONE},
	{case_12, "{\"key\":{\"subkey\":false,\"subkey2\":true}}", RJP_FORMAT_NONE},
	{case_13, "{\"key\":{\"subkey2\":true,\"subkey\":false}}", RJP_FORMAT_NONE},
	{case_14, "[5]", RJP_FORMAT_NONE},
	{case_15, "[[false]]", RJP_FORMAT_NONE},
	{case_16, "[[false,true]]", RJP_FORMAT_NONE},
	{case_17, "[5,[false,true]]", RJP_FORMAT_NONE},
	{case_18, "[5,{\"key\":[false]}]", RJP_FORMAT_NONE},
	{case_19, "[5,{\"key0\":true,\"key1\":false,\"key2\":true,\"key3\":false,\"key4\":true,\"key5\":false,\"key6\":true,\"key7\":false,\"key8\":true,\"key9\":false}]", RJP_FORMAT_NONE},
	{case_20, "[5,{\"key9\":true,\"key8\":false,\"key7\":true,\"key6\":false,\"key5\":true,\"key4\":false,\"key3\":true,\"key2\":false,\"key1\":true,\"key0\":false}]", RJP_FORMAT_NONE},
	{case_21, "{\"arr\":[0,1,2,3,4,5,6,7,8,9]}", RJP_FORMAT_NONE},
	{case_22, "{\"key\":{\"subkey\":false,\"subkey2\":true}}", RJP_FORMAT_NONE},
	{case_23, "{\"key\":{\"subkey\":false,\"subkey2\":true}}", RJP_FORMAT_NONE},
	{case_24, "{\"key\":{\"subkey\":false,\"subkey2\":true}}", RJP_FORMAT_NONE},
	{case_25, "{\"key\":{\"subkey2\":true,\"subkey\":false}}", RJP_FORMAT_NONE},
	{case_26, "\"string\"", RJP_FORMAT_NONE},
	{case_27, "\"\"", RJP_FORMAT_NONE},
	{case_28, "{\"key\":\"string\"}", RJP_FORMAT_NONE},
	{case_29, "{\"key\":\"\"}", RJP_FORMAT_NONE},

	{case_8, "{\"key\":7}", RJP_FORMAT_COMMA_SPACES},
	{case_10, "{\"key\":{\"subkey\":false, \"subkey2\":true}}", RJP_FORMAT_COMMA_SPACES},
	{case_11, "{\"key\":{\"subkey\":false, \"subkey2\":true}}", RJP_FORMAT_COMMA_SPACES},
	{case_12, "{\"key\":{\"subkey\":false, \"subkey2\":true}}", RJP_FORMAT_COMMA_SPACES},
	{case_13, "{\"key\":{\"subkey2\":true, \"subkey\":false}}", RJP_FORMAT_COMMA_SPACES},
	{case_14, "[5]", RJP_FORMAT_COMMA_SPACES},
	{case_16, "[[false, true]]", RJP_FORMAT_COMMA_SPACES},
	{case_17, "[5, [false, true]]", RJP_FORMAT_COMMA_SPACES},
	{case_18, "[5, {\"key\":[false]}]", RJP_FORMAT_COMMA_SPACES},
	{case_19, "[5, {\"key0\":true, \"key1\":false, \"key2\":true, \"key3\":false, \"key4\":true, \"key5\":false, \"key6\":true, \"key7\":false, \"key8\":true, \"key9\":false}]", RJP_FORMAT_COMMA_SPACES},
	{case_20, "[5, {\"key9\":true, \"key8\":false, \"key7\":true, \"key6\":false, \"key5\":true, \"key4\":false, \"key3\":true, \"key2\":false, \"key1\":true, \"key0\":false}]", RJP_FORMAT_COMMA_SPACES},
	{case_21, "{\"arr\":[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]}", RJP_FORMAT_COMMA_SPACES},
	{case_22, "{\"key\":{\"subkey\":false, \"subkey2\":true}}", RJP_FORMAT_COMMA_SPACES},
	{case_23, "{\"key\":{\"subkey\":false, \"subkey2\":true}}", RJP_FORMAT_COMMA_SPACES},
	{case_24, "{\"key\":{\"subkey\":false, \"subkey2\":true}}", RJP_FORMAT_COMMA_SPACES},
	{case_25, "{\"key\":{\"subkey2\":true, \"subkey\":false}}", RJP_FORMAT_COMMA_SPACES},

	{case_8, "{\"key\": 7}", RJP_FORMAT_KEY_SPACES},
	{case_9, "{\"key\": {\"subkey\": false}}", RJP_FORMAT_KEY_SPACES},
	{case_10, "{\"key\": {\"subkey\": false,\"subkey2\": true}}", RJP_FORMAT_KEY_SPACES},
	{case_11, "{\"key\": {\"subkey\": false,\"subkey2\": true}}", RJP_FORMAT_KEY_SPACES},
	{case_12, "{\"key\": {\"subkey\": false,\"subkey2\": true}}", RJP_FORMAT_KEY_SPACES},
	{case_13, "{\"key\": {\"subkey2\": true,\"subkey\": false}}", RJP_FORMAT_KEY_SPACES},
	{case_18, "[5,{\"key\": [false]}]", RJP_FORMAT_KEY_SPACES},
	{case_19, "[5,{\"key0\": true,\"key1\": false,\"key2\": true,\"key3\": false,\"key4\": true,\"key5\": false,\"key6\": true,\"key7\": false,\"key8\": true,\"key9\": false}]", RJP_FORMAT_KEY_SPACES},
	{case_20, "[5,{\"key9\": true,\"key8\": false,\"key7\": true,\"key6\": false,\"key5\": true,\"key4\": false,\"key3\": true,\"key2\": false,\"key1\": true,\"key0\": false}]", RJP_FORMAT_KEY_SPACES},
	{case_21, "{\"arr\": [0,1,2,3,4,5,6,7,8,9]}", RJP_FORMAT_KEY_SPACES},
	{case_22, "{\"key\": {\"subkey\": false,\"subkey2\": true}}", RJP_FORMAT_KEY_SPACES},
	{case_23, "{\"key\": {\"subkey\": false,\"subkey2\": true}}", RJP_FORMAT_KEY_SPACES},
	{case_24, "{\"key\": {\"subkey\": false,\"subkey2\": true}}", RJP_FORMAT_KEY_SPACES},
	{case_25, "{\"key\": {\"subkey2\": true,\"subkey\": false}}", RJP_FORMAT_KEY_SPACES},
	{case_28, "{\"key\": \"string\"}", RJP_FORMAT_KEY_SPACES},
	{case_29, "{\"key\": \"\"}", RJP_FORMAT_KEY_SPACES},

	{case_5, "{}", RJP_FORMAT_TABBED_LINES},
	{case_6, "[]", RJP_FORMAT_TABBED_LINES},
	{case_7, "{}", RJP_FORMAT_TABBED_LINES},
	{case_8, "{\n\t\"key\":7\n}", RJP_FORMAT_TABBED_LINES},
	{case_9, "{\n\t\"key\":{\n\t\t\"subkey\":false\n\t}\n}", RJP_FORMAT_TABBED_LINES},
	{case_10, "{\n\t\"key\":{\n\t\t\"subkey\":false,\n\t\t\"subkey2\":true\n\t}\n}", RJP_FORMAT_TABBED_LINES},
	{case_11, "{\n\t\"key\":{\n\t\t\"subkey\":false,\n\t\t\"subkey2\":true\n\t}\n}", RJP_FORMAT_TABBED_LINES},
	{case_12, "{\n\t\"key\":{\n\t\t\"subkey\":false,\n\t\t\"subkey2\":true\n\t}\n}", RJP_FORMAT_TABBED_LINES},
	{case_13, "{\n\t\"key\":{\n\t\t\"subkey2\":true,\n\t\t\"subkey\":false\n\t}\n}", RJP_FORMAT_TABBED_LINES},
	{case_14, "[\n\t5\n]", RJP_FORMAT_TABBED_LINES},
	{case_15, "[\n\t[\n\t\tfalse\n\t]\n]", RJP_FORMAT_TABBED_LINES},
	{case_16, "[\n\t[\n\t\tfalse,\n\t\ttrue\n\t]\n]", RJP_FORMAT_TABBED_LINES},
	{case_17, "[\n\t5,\n\t[\n\t\tfalse,\n\t\ttrue\n\t]\n]", RJP_FORMAT_TABBED_LINES},
	{case_18, "[\n\t5,\n\t{\n\t\t\"key\":[\n\t\t\tfalse\n\t\t]\n\t}\n]", RJP_FORMAT_TABBED_LINES},
	{case_19, "[\n\t5,\n\t{\n\t\t\"key0\":true,\n\t\t\"key1\":false,\n\t\t\"key2\":true,\n\t\t\"key3\":false,\n\t\t\"key4\":true,\n\t\t\"key5\":false,\n\t\t\"key6\":true,\n\t\t\"key7\":false,\n\t\t\"key8\":true,\n\t\t\"key9\":false\n\t}\n]", RJP_FORMAT_TABBED_LINES},
	{case_20, "[\n\t5,\n\t{\n\t\t\"key9\":true,\n\t\t\"key8\":false,\n\t\t\"key7\":true,\n\t\t\"key6\":false,\n\t\t\"key5\":true,\n\t\t\"key4\":false,\n\t\t\"key3\":true,\n\t\t\"key2\":false,\n\t\t\"key1\":true,\n\t\t\"key0\":false\n\t}\n]", RJP_FORMAT_TABBED_LINES},
	{case_21, "{\n\t\"arr\":[\n\t\t0,\n\t\t1,\n\t\t2,\n\t\t3,\n\t\t4,\n\t\t5,\n\t\t6,\n\t\t7,\n\t\t8,\n\t\t9\n\t]\n}", RJP_FORMAT_TABBED_LINES},
	{case_22, "{\n\t\"key\":{\n\t\t\"subkey\":false,\n\t\t\"subkey2\":true\n\t}\n}", RJP_FORMAT_TABBED_LINES},
	{case_23, "{\n\t\"key\":{\n\t\t\"subkey\":false,\n\t\t\"subkey2\":true\n\t}\n}", RJP_FORMAT_TABBED_LINES},
	{case_24, "{\n\t\"key\":{\n\t\t\"subkey\":false,\n\t\t\"subkey2\":true\n\t}\n}", RJP_FORMAT_TABBED_LINES},
	{case_25, "{\n\t\"key\":{\n\t\t\"subkey2\":true,\n\t\t\"subkey\":false\n\t}\n}", RJP_FORMAT_TABBED_LINES},
	{case_28, "{\n\t\"key\":\"string\"\n}", RJP_FORMAT_TABBED_LINES},
	{case_29, "{\n\t\"key\":\"\"\n}", RJP_FORMAT_TABBED_LINES},

	{case_1, "null", RJP_FORMAT_PRETTY},
	{case_2, "5", RJP_FORMAT_PRETTY},
	{case_3, "true", RJP_FORMAT_PRETTY},
	{case_4, "false", RJP_FORMAT_PRETTY},
	{case_5, "{}", RJP_FORMAT_PRETTY},
	{case_6, "[]", RJP_FORMAT_PRETTY},
	{case_7, "{}", RJP_FORMAT_PRETTY},
	{case_8, "{\n\t\"key\": 7\n}", RJP_FORMAT_PRETTY},
	{case_9, "{\n\t\"key\": {\n\t\t\"subkey\": false\n\t}\n}", RJP_FORMAT_PRETTY},
	{case_10, "{\n\t\"key\": {\n\t\t\"subkey\": false,\n\t\t\"subkey2\": true\n\t}\n}", RJP_FORMAT_PRETTY},
	{case_11, "{\n\t\"key\": {\n\t\t\"subkey\": false,\n\t\t\"subkey2\": true\n\t}\n}", RJP_FORMAT_PRETTY},
	{case_12, "{\n\t\"key\": {\n\t\t\"subkey\": false,\n\t\t\"subkey2\": true\n\t}\n}", RJP_FORMAT_PRETTY},
	{case_13, "{\n\t\"key\": {\n\t\t\"subkey2\": true,\n\t\t\"subkey\": false\n\t}\n}", RJP_FORMAT_PRETTY},
	{case_14, "[\n\t5\n]", RJP_FORMAT_PRETTY},
	{case_15, "[\n\t[\n\t\tfalse\n\t]\n]", RJP_FORMAT_PRETTY},
	{case_16, "[\n\t[\n\t\tfalse,\n\t\ttrue\n\t]\n]", RJP_FORMAT_PRETTY},
	{case_17, "[\n\t5,\n\t[\n\t\tfalse,\n\t\ttrue\n\t]\n]", RJP_FORMAT_PRETTY},
	{case_18, "[\n\t5,\n\t{\n\t\t\"key\": [\n\t\t\tfalse\n\t\t]\n\t}\n]", RJP_FORMAT_PRETTY},
	{case_19, "[\n\t5,\n\t{\n\t\t\"key0\": true,\n\t\t\"key1\": false,\n\t\t\"key2\": true,\n\t\t\"key3\": false,\n\t\t\"key4\": true,\n\t\t\"key5\": false,\n\t\t\"key6\": true,\n\t\t\"key7\": false,\n\t\t\"key8\": true,\n\t\t\"key9\": false\n\t}\n]", RJP_FORMAT_PRETTY},
	{case_20, "[\n\t5,\n\t{\n\t\t\"key9\": true,\n\t\t\"key8\": false,\n\t\t\"key7\": true,\n\t\t\"key6\": false,\n\t\t\"key5\": true,\n\t\t\"key4\": false,\n\t\t\"key3\": true,\n\t\t\"key2\": false,\n\t\t\"key1\": true,\n\t\t\"key0\": false\n\t}\n]", RJP_FORMAT_PRETTY},
	{case_21, "{\n\t\"arr\": [\n\t\t0,\n\t\t1,\n\t\t2,\n\t\t3,\n\t\t4,\n\t\t5,\n\t\t6,\n\t\t7,\n\t\t8,\n\t\t9\n\t]\n}", RJP_FORMAT_PRETTY},
	{case_22, "{\n\t\"key\": {\n\t\t\"subkey\": false,\n\t\t\"subkey2\": true\n\t}\n}", RJP_FORMAT_PRETTY},
	{case_23, "{\n\t\"key\": {\n\t\t\"subkey\": false,\n\t\t\"subkey2\": true\n\t}\n}", RJP_FORMAT_PRETTY},
	{case_24, "{\n\t\"key\": {\n\t\t\"subkey\": false,\n\t\t\"subkey2\": true\n\t}\n}", RJP_FORMAT_PRETTY},
	{case_25, "{\n\t\"key\": {\n\t\t\"subkey2\": true,\n\t\t\"subkey\": false\n\t}\n}", RJP_FORMAT_PRETTY},
	{case_26, "\"string\"", RJP_FORMAT_PRETTY},
	{case_27, "\"\"", RJP_FORMAT_PRETTY},
	{case_28, "{\n\t\"key\": \"string\"\n}", RJP_FORMAT_PRETTY},
	{case_29, "{\n\t\"key\": \"\"\n}", RJP_FORMAT_PRETTY},
};

int run_test(test_pair* p){
	RJP_value* test = p->create();
	char* buf = rjp_to_json(test, p->fmt);
	if(!buf){
		if(!p->res)
			return 1;
		return 0;
	}
	int retval = !strcmp(buf, p->res);
	if(retval){
		fprintf(stderr, "Success\n");
	}else{
		fprintf(stderr, "Failure\n");
		fprintf(stderr, "Expected: '%s'\nGot:      '%s'\n", p->res, buf);
	}
	rjp_free(buf);
	rjp_free_value(test);
	return retval;
}

int main(){
	const int num_tests = sizeof(tests) / sizeof(tests[0]);
	int passed = 0;
	fprintf(stderr, "Running %d tests that should succeed...\n", num_tests);
	for(int i = 0;i < num_tests;++i){
		fprintf(stderr, "%8d)    ", i+1);
		if(run_test(&tests[i])){
			++passed;
		}
	}
	fprintf(stderr, "\nResults: %d/%d tests passed\n", passed, num_tests);
	if(passed != num_tests)
		return 1;
	return 0;
}

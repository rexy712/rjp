# README

## About
rjp (rexy's json parser) is a very simplistic json parser written in C. There is absolutely no reason for this to exist but I felt like making it :P

Also includes rjp++, a C++ wrapper around the core rjp library. Building of this library can be disabled with cmake and is currently impossible to build since I haven't released librexy to my public gitlab yet. Remind me to update this readme when I do release that library.

## Building
Building will produce a shared library by default but can be configured to build a static library.

##### Run the following commands
```
mkdir build
cd build
ccmake ..
#or jest 'cmake ..' and specify options on the command line
make
```

## Installing
##### Run the following commands from the bulid directory after building
```
sudo make install
```

## Uninstalling
##### Run the following commands from the build directory
```
sudo make uninstall
```

/**
	rjp
	Copyright (C) 2018-2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "rjp_internal.h"
#include "rjp_string.h"
#include "rjp_value.h"

#define __STDC_FORMAT_MACROS
#include <inttypes.h> //PRId64
#include <stdio.h> //fprintf
#include <stdint.h> //uintN_t
#include <string.h> //strcpy

//Determine if the character is valid whitespace
int irjp_is_whitespace(char c){
	return c == ' ' || c == '\n' || c == '\r' || c == '\t';
}

static uint32_t utf_strtol_4(const char* c){
	uint32_t ret = 0;
	for(RJP_index i = 0;i < 4;++i){
		if(c[i] >= '0' && c[i] <= '9'){
			ret |= ((c[i] ^ 0x30) << (4*(3-i)));
		}else if(c[i] >= 'A' && c[i] <= 'F'){
			ret |= ((c[i] - 0x37) << (4*(3-i)));
		}else if(c[i] >= 'a' && c[i] <= 'f'){
			ret |= ((c[i] - 0x57) << (4*(3-i)));
		}else{
			return 0;
		}
	}
	return ret;
}


static int decode_unicode_escape(const char* str, uint32_t* high, uint32_t* low){
	if(*str != '\\' || *(str+1) != 'u'){ //invalid
		return *low = *high = 0;
	}
	*high = utf_strtol_4(str+2);
	if(!*high)
		return *low = *high = 0;
	if((*high & 0xF800) == 0xD800){ //utf-16
		if(*(str+6) != '\\' || *(str+7) != 'u'){
			return *low = *high = 0;
		}
		*low = utf_strtol_4(str+8);
		if(!*low)
			return *low = *high = 0;
		return 12;
	}else{
		*low = 0;
	}
	return 6;
}

static uint32_t u16_surrogate_pair_to_codepoint(uint32_t high, uint32_t low){
	uint32_t codepoint;

	codepoint = ((high & 0x07FF) << 10) | (1 << 16);
	codepoint = codepoint | (low & 0x03FF);

	return codepoint;
}

static uint32_t utf_to_codepoint(uint32_t high, uint32_t low){
	if(!low) //utf8
		return high;
	return u16_surrogate_pair_to_codepoint(high, low);
}
static int codepoint_strlen(uint32_t codepoint){
	if(codepoint <= 0x007F){
		return 1;
	}else if(codepoint <= 0x07FF){
		return 2;
	}else if(codepoint <= 0xFFFF){
		return 3;
	}else if(codepoint <= 0x10FFFF){
		return 4;
	}else{
		return 0;
	}
}

static int codepoint_to_u8(char* dest, uint32_t codepoint){
	if(codepoint <= 0x007F){
		dest[0] = codepoint;
		return 1;
	}else if(codepoint <= 0x07FF){
		dest[0] = (codepoint >> 6) | 0xC0;
		dest[1] = (codepoint & 0x3F) | 0x80;
		return 2;
	}else if(codepoint <= 0xFFFF){
		dest[0] = (codepoint >> 12) | 0xE0;
		dest[1] = ((codepoint >> 6) & 0x3F) | 0x80;
		dest[2] = (codepoint & 0x3F) | 0x80;
		return 3;
	}else if(codepoint <= 0x10FFFF){
		dest[0] = (codepoint >> 18) | 0xF0;
		dest[1] = ((codepoint >> 12) & 0x3F) | 0x80;
		dest[2] = ((codepoint >> 6) & 0x3F) | 0x80;
		dest[3] = (codepoint & 0x3F) | 0x80;
		return 4;
	}else{
		return 0;
	}
}
/*static uint32_t u8_to_codepoint(char* u){
	if((u[0] & 0x80) == 0){
		//one byte
		return u[0];
	}else if((u[0] & 0xE0) == 0xC0){
		//two byte
		uint32_t codepoint;
		codepoint = (u[0] & 0x1F) << 6;
		codepoint |= (u[1] & 0x3F);
		return codepoint;
	}else if((u[0] & 0xF0) == 0xE0){
		//three byte
		uint32_t codepoint;
		codepoint = (u[0] & 0x0F) << 12;
		codepoint |= (u[1] & 0x3F) << 6;
		codepoint |= (u[2] & 0x3F);
		return codepoint;
	}else if((u[0] & 0xF8) == 0xF0){
		//four byte
		uint32_t codepoint;
		codepoint = (u[0] & 0x07) << 18;
		codepoint |= (u[1] & 0x3F) << 12;
		codepoint |= (u[2] & 0x3F) << 6;
		codepoint |= (u[3] & 0x3F);
		return codepoint;
	}else{
		//invalid
		return 0;
	}
}*/

int irjp_copy_string_quoted(char* restrict dest, const char* restrict string, RJP_index length){
	dest[0] = '"';
	memcpy(dest+1, string, length);
	dest[length+1] = '"';
	return length+2;
}
int irjp_copy_string_keyed(char* restrict dest, const char* restrict string, RJP_index length){
	int offset = irjp_copy_string_quoted(dest, string, length);
	dest[offset] = ':';
	return offset+1;
}
char* irjp_convert_string(const char* str, RJP_index length, RJP_index* newlen, const RJP_memory_fns* fns){
	char* newstring;
	RJP_index oldpos = 1; //ignore opening quote
	--length; //ignore closing quote
	RJP_index newlength = 0;
	for(;oldpos < length;++newlength,++oldpos){
		if(str[oldpos] == '\\'){
			if(str[oldpos+1] == 'u'){
				//unicode escape
				uint32_t high, low;
				oldpos += (decode_unicode_escape(str+oldpos, &high, &low)-1);
				if(high == 0)
					return NULL;
				newlength += (codepoint_strlen(utf_to_codepoint(high, low))-1);
			}else{
				++oldpos;
			}
		}else if(str[oldpos] == 0){
			return NULL;
		}
	}
	newstring = fns->alloc(newlength + 1);
	newstring[newlength] = 0;
	*newlen = newlength;
	oldpos = 1;
	for(RJP_index newpos = 0;oldpos < length;++newpos,++oldpos){
		if(str[oldpos] == '\\'){
			++oldpos;
			switch(str[oldpos]){
			case '"':
				newstring[newpos] = '"';
				break;
			case 'n':
				newstring[newpos] = '\n';
				break;
			case 'r':
				newstring[newpos] = '\r';
				break;
			case 'b':
				newstring[newpos] = '\b';
				break;
			case '\\':
				newstring[newpos] = '\\';
				break;
			case 't':
				newstring[newpos] = '\t';
				break;
			case 'f':
				newstring[newpos] = '\f';
				break;
			case 'u':;
				uint32_t high, low;
				uint32_t codepoint;
				--oldpos;
				oldpos += (decode_unicode_escape(str+oldpos, &high, &low)-1);
				if(!high){
					rjp_free(newstring);
					return NULL;
				}
				codepoint = utf_to_codepoint(high, low);
				newpos += (codepoint_to_u8(newstring+newpos, codepoint)-1);
				break;
			default:
				newstring[newpos] = str[oldpos];
				break;
			};
		}else{
			newstring[newpos] = str[oldpos];
		}
	}
	return newstring;
}

RJP_index rjp_escape_strcpy(char* dest, const char* src){
	RJP_index j = 0;
	for(RJP_index i = 0;src[i];++i){
		switch(src[i]){
		case '"':
			dest[j++] = '\\';
			dest[j++] = '"';
			break;
		case '\n':
			dest[j++] = '\\';
			dest[j++] = 'n';
			break;
		case '\r':
			dest[j++] = '\\';
			dest[j++] = 'r';
			break;
		case '\b':
			dest[j++] = '\\';
			dest[j++] = 'b';
			break;
		case '\\':
			dest[j++] = '\\';
			dest[j++] = '\\';
			break;
		case '\t':
			dest[j++] = '\\';
			dest[j++] = 't';
			break;
		case '\f':
			dest[j++] = '\\';
			dest[j++] = 'f';
			break;
		default:
			dest[j++] = src[i];
			break;
		};
	}
	dest[j] = 0;
	return j;
}
void irjp_strcpy(RJP_string* dest, const RJP_string* src, const RJP_memory_fns* fns){
	dest->value = fns->alloc(src->length + 1);
	strcpy(dest->value, src->value);
	dest->value[src->length] = 0;
	dest->length = src->length;
}

RJP_index rjp_escape_strlen(const char* str){
	RJP_index count = 0;
	for(RJP_index i = 0;str[i];++i){
		switch(str[i]){
		case '"':
		case '\n':
		case '\r':
		case '\b':
		case '\\':
		case '\t':
		case '\f':
			++count;
			//fallthrough
		default:
			++count;
		};
	}
	return count;
}
RJP_string rjp_escape(const char* src){
	RJP_index esclen = rjp_escape_strlen(src);
	char* dest = rjp_alloc(esclen+1);
	rjp_escape_strcpy(dest, src);
	return (RJP_string){.value = dest, .length = esclen};
}
RJP_index irjp_array_strlen(const RJP_value* arr, const int flags, int depth){
	RJP_index count = 2; //[]
	if(flags & RJP_FORMAT_NEWLINES)
		++count;
	if(flags & RJP_FORMAT_TABBED_LINES)
		count += depth;
	++depth;

	RJP_array_iterator it;
	rjp_init_array_iterator(&it, arr);
	for(RJP_value* current = rjp_array_iterator_current(&it);current;current = rjp_array_iterator_next(&it)){
		count += irjp_value_strlen(current, flags, depth);
		if(flags & RJP_FORMAT_NEWLINES)
			++count;
		if(flags & RJP_FORMAT_TABBED_LINES)
			count += depth;
		if(rjp_array_iterator_peek(&it)){
			++count; //,
			if(flags & RJP_FORMAT_COMMA_SPACES)
				++count; //space
		}
	}
	return count;
}

RJP_index irjp_object_strlen(const RJP_value* root, const int flags, int depth){
	RJP_index count = 2; //{}
	if(flags & RJP_FORMAT_NEWLINES)
		++count;
	if(flags & RJP_FORMAT_TABBED_LINES)
		count += depth;
	++depth;

	RJP_object_iterator it;
	rjp_init_object_iterator(&it, root);
	RJP_value* current = rjp_object_iterator_current(&it);
	while(current){
		RJP_index namelen = rjp_member_key(current)->length;
		count += namelen + 3; //"":
		if(flags & RJP_FORMAT_KEY_SPACES)
			++count;
		count += irjp_value_strlen(current, flags, depth);
		if(flags & RJP_FORMAT_NEWLINES)
			++count;
		if(flags & RJP_FORMAT_TABBED_LINES)
			count += depth;
		if((current = rjp_object_iterator_next(&it))){
			++count; //,
			if(flags & RJP_FORMAT_COMMA_SPACES)
				++count; //space
		}
	}
	rjp_delete_object_iterator(&it);
	return count;
}

RJP_index irjp_value_strlen(const RJP_value* root, const int flags, int depth){
	switch(root->type){
	case rjp_json_integer:
		return snprintf(NULL, 0, "%" PRId64, root->integer);
	case rjp_json_dfloat:
		return snprintf(NULL, 0, "%lf", root->dfloat);
	case rjp_json_boolean:
		return root->boolean ? 4 : 5; //true, false
	case rjp_json_null:
		return 4;
	case rjp_json_string:
		return rjp_escape_strlen(root->string.value) + 2; //""
	case rjp_json_array:
		return irjp_array_strlen(root, flags, depth);
	case rjp_json_object:
	case rjp_json_ordered_object:
		return irjp_object_strlen(root, flags, depth);
	default:
		return 0;
	};
}

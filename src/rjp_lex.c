/**
	rjp
	Copyright (C) 2018-2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "rjp_lex.h"
#include "rjp.h"

#include <ctype.h> //isalpha, etc
#include <string.h> //memcpy

void irjp_init_lex_cback_state(RJP_lex_state* state){
	state->str = rjp_alloc(RJP_LEX_CBACK_STR_SIZE+1);
	state->str[RJP_LEX_CBACK_STR_SIZE] = 0;
	state->strcap = RJP_LEX_CBACK_STR_SIZE;
	state->buff = rjp_alloc(RJP_LEX_CBACK_BUFFER_SIZE);
	state->buffcap = RJP_LEX_CBACK_BUFFER_SIZE;
	state->type = RJP_LEX_TYPE_CBACK;
}
void irjp_init_lex_state(RJP_lex_state* state){
	state->type = RJP_LEX_TYPE_NORMAL;
}
void irjp_delete_lex_state(RJP_lex_state* state){
	if(state->type == RJP_LEX_TYPE_CBACK){
		rjp_free(state->str);
		rjp_free(state->buff);
	}
}

static RJP_lex_category irjp_lex_accept(RJP_lex_category val, RJP_lex_state* state){
	state->node = rjp_lex_start;
	return val;
}
static int irjp_is_space(char ch){
	switch(ch){
	case ' ':
	case '\t':
	case '\f':
	case '\v':
		return 1;
	};
	return 0;
}
static inline RJP_lex_category irjp_lex_do_start(char ch){
	switch(ch){
	case 0:
		return rjp_lex_end;
	case '{':
		return rjp_lex_obrace;
	case '}':
		return rjp_lex_cbrace;
	case '[':
		return rjp_lex_obracket;
	case ']':
		return rjp_lex_cbracket;
	case '"':
		return rjp_lex_quote;
	case ',':
		return rjp_lex_comma;
	case ':':
		return rjp_lex_colon;
	case 't':
		return rjp_lex_t;
	case 'f':
		return rjp_lex_f;
	case 'n':
		return rjp_lex_n;
	case '/':
		return rjp_lex_slash;
	case '+':
	case '-':
		return rjp_lex_signed_number;
	case '\n':
	case '\r':
		return rjp_lex_newlines;
	case ' ':
	case '\t':
	case '\v':
	case '\f':
		return rjp_lex_spaces;
	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
		return rjp_lex_number;
	default:
		break;
	};
	return rjp_lex_invalid;
}
static inline RJP_lex_category irjp_lex_do_spaces(char ch){
	if(irjp_is_space(ch))
		return rjp_lex_spaces;
	return rjp_lex_invalid;
}
static inline RJP_lex_category irjp_lex_do_number(char ch){
	if(isdigit(ch))
		return rjp_lex_number;
	if(ch == '.')
		return rjp_lex_decimal;
	if(isalpha(ch))
		return rjp_lex_unrecognized_word;
	return rjp_lex_invalid;
}
static inline RJP_lex_category irjp_lex_do_signed_num(char ch){
	switch(ch){
	case '-':
	case '+':
		return rjp_lex_number;
	};
	return irjp_lex_do_number(ch);
}
static inline RJP_lex_category irjp_lex_do_decimal(char ch){
	if(isdigit(ch))
		return rjp_lex_fnumber;
	return rjp_lex_invalid;
}
static inline RJP_lex_category irjp_lex_do_fnumber(char ch){
	if(isdigit(ch))
		return rjp_lex_fnumber;
	if(ch == 'e' || ch == 'E')
		return rjp_lex_fnum_e;
	if(isalpha(ch))
		return rjp_lex_unrecognized_word;
	return rjp_lex_invalid;
}
static inline RJP_lex_category irjp_lex_do_fnum_e(char ch){
	if(ch == '-' || ch == '+')
		return rjp_lex_sci_num_signed;
	if(isdigit(ch))
		return rjp_lex_sci_num;
	return rjp_lex_invalid;
}
static inline RJP_lex_category irjp_lex_do_sci_num_signed(char ch){
	if(isdigit(ch))
		return rjp_lex_sci_num;
	return rjp_lex_invalid;
}
static inline RJP_lex_category irjp_lex_do_sci_num(char ch){
	if(isdigit(ch))
		return rjp_lex_sci_num;
	if(isalpha(ch))
		return rjp_lex_unrecognized_word;
	return rjp_lex_invalid;
}
static inline RJP_lex_category irjp_lex_do_quote(char ch){
	switch(ch){
	case '\\':
		return rjp_lex_escaped;
	case '\n':
	case '\r':
		return rjp_lex_invalid;
	case '"':
		return rjp_lex_string;
	};
	return rjp_lex_quote;
}
static inline RJP_lex_category irjp_lex_do_slash(char ch){
	switch(ch){
	case '/':
		return rjp_lex_line_comment;
	case '*':
		return rjp_lex_block_comment_start;
	};
	return rjp_lex_invalid;
}
static inline RJP_lex_category irjp_lex_do_line_comment(char ch){
	switch(ch){
	case '\n':
	case '\r':
	case 0:
		return rjp_lex_invalid;
	};
	return rjp_lex_line_comment;
}
static inline RJP_lex_category irjp_lex_do_block_comment_start(char ch){
	if(ch == '*')
		return rjp_lex_block_comment_end1;
	return rjp_lex_block_comment_start;
}
static inline RJP_lex_category irjp_lex_do_block_comment_end1(char ch){
	if(ch == '/')
		return rjp_lex_block_comment;
	return rjp_lex_block_comment_start;
}
static RJP_lex_category irjp_lex_char(char ch, RJP_lex_category node){
	switch(node){
	case rjp_lex_start:
		return irjp_lex_do_start(ch);
	//whitespace
	case rjp_lex_spaces:
		return irjp_lex_do_spaces(ch);

	//numbers
	case rjp_lex_signed_number:
		return irjp_lex_do_signed_num(ch);
	case rjp_lex_number:
		return irjp_lex_do_number(ch);
	case rjp_lex_decimal:
		return irjp_lex_do_decimal(ch);
	case rjp_lex_fnumber:
		return irjp_lex_do_fnumber(ch);
	case rjp_lex_fnum_e:
		return irjp_lex_do_fnum_e(ch);
	case rjp_lex_sci_num_signed:
		return irjp_lex_do_sci_num_signed(ch);
	case rjp_lex_sci_num:
		return irjp_lex_do_sci_num(ch);

	//strings
	case rjp_lex_quote:
		return irjp_lex_do_quote(ch);
	case rjp_lex_escaped:
		return rjp_lex_quote;

	//comments
	case rjp_lex_slash:
		return irjp_lex_do_slash(ch);
	case rjp_lex_line_comment:
		return irjp_lex_do_line_comment(ch);
	case rjp_lex_block_comment_start:
		return irjp_lex_do_block_comment_start(ch);
	case rjp_lex_block_comment_end1:
		return irjp_lex_do_block_comment_end1(ch);

	//true
	case rjp_lex_t:
		if(ch != 'r')
			return rjp_lex_unrecognized_word;
		return rjp_lex_tr;
	case rjp_lex_tr:
		if(ch != 'u')
			return rjp_lex_unrecognized_word;
		return rjp_lex_tru;
	case rjp_lex_tru:
		if(ch != 'e')
			return rjp_lex_unrecognized_word;
		return rjp_lex_true;

	//false
	case rjp_lex_f:
		if(ch != 'a')
			return rjp_lex_unrecognized_word;
		return rjp_lex_fa;
	case rjp_lex_fa:
		if(ch != 'l')
			return rjp_lex_unrecognized_word;
		return rjp_lex_fal;
	case rjp_lex_fal:
		if(ch != 's')
			return rjp_lex_unrecognized_word;
		return rjp_lex_fals;
	case rjp_lex_fals:
		if(ch != 'e')
			return rjp_lex_unrecognized_word;
		return rjp_lex_false;

	//null
	case rjp_lex_n:
		if(ch != 'u')
			return rjp_lex_unrecognized_word;
		return rjp_lex_nu;
	case rjp_lex_nu:
		if(ch != 'l')
			return rjp_lex_unrecognized_word;
		return rjp_lex_nul;
	case rjp_lex_nul:
		if(ch != 'l')
			return rjp_lex_unrecognized_word;
		return rjp_lex_null;

	case rjp_lex_true:
	case rjp_lex_false:
	case rjp_lex_null:
		if(!isalnum(ch))
			return rjp_lex_invalid;
		return rjp_lex_unrecognized_word;

	case rjp_lex_unrecognized_word:
		if(isalnum(ch))
			return rjp_lex_unrecognized_word;
		return rjp_lex_invalid;
	default:
		return rjp_lex_invalid;
	};
	return node;
}
//straight forward lex. All json data in single string
//use state->str as constant string. index into it using state->offset and
//state->length to acquire tokens in the parser
RJP_lex_category irjp_lex(RJP_lex_state* state){
	state->offset += state->length;
	state->length = 0;
	for(const char* c = state->str+state->offset;1;++c,++state->length){
		RJP_lex_category cat = irjp_lex_char(*c, state->node);
		if(cat == rjp_lex_invalid)
			return irjp_lex_accept(state->node, state);
		state->node = cat;
		if(*c == 0)
			break;
	}
	return irjp_lex_accept(state->node, state);
}

static void irjp_lex_resize_strbuf(RJP_lex_state* state, int newsize){
	char* newbuf = rjp_alloc(newsize+1);
	memcpy(newbuf, state->str, state->strcap);
	newbuf[newsize] = 0;
	rjp_free(state->str);
	state->str = newbuf;
	state->strcap = newsize;
}
//user callback based lexer. Not all json data is available at one time.
//Tokens need saved in a secondary buffer for the parser to have access.
//state->str is where the secondary buffer is located and state->length
//is used to track its size. state->offset MUST be 0 for parser to get proper
//token values
RJP_lex_category irjp_lex_cback(RJP_lex_state* state, RJP_parse_callback* cbacks){
	state->length = 0;
	//pick up from previous invocation
	RJP_index chars_read = state->buffl;
	if(chars_read == 0){
		state->buffpos = 0;
		chars_read = cbacks->read(state->buff, state->buffcap, cbacks->data);
		state->buffl = chars_read;
	}

	//loop until callback returns 0 new chars
	while(chars_read > 0){
		//loop over all characters in current buffer
		for(RJP_index i = 0;(i+state->buffpos) < chars_read;++i,++state->length){
			if(state->length == state->strcap) //need more space to store lex token
				irjp_lex_resize_strbuf(state, state->strcap*2);
			RJP_lex_category cat = irjp_lex_char(state->buff[state->buffpos+i], state->node);
			if(cat == rjp_lex_invalid){
				//save necessary state and return previous state
				state->buffpos = i + state->buffpos;
				state->str[state->length] = 0;
				return irjp_lex_accept(state->node, state);
			}
			state->str[state->length] = state->buff[state->buffpos+i];
			state->node = cat;
		}
		//read new values into buffer, reset buffer related state
		chars_read = cbacks->read(state->buff, state->buffcap, cbacks->data);
		state->buffpos = 0;
		state->buffl = chars_read;
	}
	//lexing cannot continue due to lack of input
	++state->buffpos;
	state->str[state->length] = 0;
	RJP_lex_category cat = state->node;
	state->node = rjp_lex_end;
	return cat;
}

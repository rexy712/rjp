/**
	rjp
	Copyright (C) 2018-2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "rjp.h"
#include "rjp_internal.h"
#include "rjp_string.h"
#include "rjp_object.h"
#include "rjp_value.h"
#include "rjp_array.h"

#define __STDC_FORMAT_MACROS
#include <inttypes.h> //PRId64
#include <string.h> //strlen
#include <stdio.h> //sprintf

RJP_value* rjp_new_null(void){
	return rjp_new_null_c(&irjp_default_memory_fns);
}
RJP_value* rjp_new_int(RJP_int val){
	return rjp_new_int_c(val, &irjp_default_memory_fns);
}
RJP_value* rjp_new_float(RJP_float val){
	return rjp_new_float_c(val, &irjp_default_memory_fns);
}
RJP_value* rjp_new_bool(RJP_bool val){
	return rjp_new_bool_c(val, &irjp_default_memory_fns);
}
RJP_value* rjp_new_string_steal(char* val, RJP_index length){
	return rjp_new_string_steal_c(val, length, &irjp_default_memory_fns);
}
RJP_value* rjp_new_string(const char* value, RJP_index length){
	return rjp_new_string_c(value, length, &irjp_default_memory_fns);
}
RJP_value* rjp_new_object(void){
	return rjp_new_object_c(&irjp_default_memory_fns);
}
RJP_value* rjp_new_ordered_object(void){
	return rjp_new_ordered_object_c(&irjp_default_memory_fns);
}
RJP_value* rjp_new_array(void){
	return rjp_new_array_c(&irjp_default_memory_fns);
}

RJP_value* rjp_new_null_c(const RJP_memory_fns* fns){
	RJP_value* ret = fns->alloc(sizeof(RJP_value));
	memset(ret, 0, sizeof(RJP_value));
	ret->type = rjp_json_null;
	return ret;
}
RJP_value* rjp_new_int_c(RJP_int val, const RJP_memory_fns* fns){
	RJP_value* ret = fns->alloc(sizeof(RJP_value));
	memset(ret, 0, sizeof(RJP_value));
	ret->type = rjp_json_integer;
	ret->integer = val;
	return ret;
}
RJP_value* rjp_new_float_c(RJP_float val, const RJP_memory_fns* fns){
	RJP_value* ret = fns->alloc(sizeof(RJP_value));
	memset(ret, 0, sizeof(RJP_value));
	ret->type = rjp_json_dfloat;
	ret->dfloat = val;
	return ret;
}
RJP_value* rjp_new_bool_c(RJP_bool val, const RJP_memory_fns* fns){
	RJP_value* ret = fns->alloc(sizeof(RJP_value));
	memset(ret, 0, sizeof(RJP_value));
	ret->type = rjp_json_boolean;
	ret->boolean = val;
	return ret;
}
RJP_value* rjp_new_string_steal_c(char* val, RJP_index length, const RJP_memory_fns* fns){
	RJP_value* ret = fns->alloc(sizeof(RJP_value));
	memset(ret, 0, sizeof(RJP_value));
	ret->type = rjp_json_string;
	ret->string.value = val;
	ret->string.length = length;
	return ret;
}
RJP_value* rjp_new_string_c(const char* value, RJP_index length, const RJP_memory_fns* fns){
	UNUSED_VARIABLE(length);
	RJP_value* ret = fns->alloc(sizeof(RJP_value));
	memset(ret, 0, sizeof(RJP_value));
	ret->type = rjp_json_string;
	RJP_index esclen = rjp_escape_strlen(value);
	ret->string.value = fns->alloc(esclen+1);
	rjp_escape_strcpy(ret->string.value, value);
	ret->string.value[esclen] = 0;
	ret->string.length = esclen;
	return ret;
}
RJP_value* rjp_new_object_c(const RJP_memory_fns* fns){
	RJP_value* ret = fns->alloc(sizeof(RJP_value));
	memset(ret, 0, sizeof(RJP_value));
	ret->type = rjp_json_object;
	return ret;
}
RJP_value* rjp_new_ordered_object_c(const RJP_memory_fns* fns){
	RJP_value* ret = fns->alloc(sizeof(RJP_value));
	memset(ret, 0, sizeof(RJP_value));
	ret->type = rjp_json_ordered_object;
	return ret;
}
RJP_value* rjp_new_array_c(const RJP_memory_fns* fns){
	RJP_value* ret = fns->alloc(sizeof(RJP_value));
	memset(ret, 0, sizeof(RJP_value));
	ret->type = rjp_json_array;
	return ret;
}

RJP_value* rjp_set_null(RJP_value* v){
	return rjp_set_null_c(v, &irjp_default_memory_fns);
}
RJP_value* rjp_set_int(RJP_value* v, RJP_int val){
	return rjp_set_int_c(v, val, &irjp_default_memory_fns);
}
RJP_value* rjp_set_float(RJP_value* v, RJP_float val){
	return rjp_set_float_c(v, val, &irjp_default_memory_fns);
}
RJP_value* rjp_set_bool(RJP_value* v, RJP_bool val){
	return rjp_set_bool_c(v, val, &irjp_default_memory_fns);
}
RJP_value* rjp_set_string_steal(RJP_value* v, char* val, RJP_index len){
	return rjp_set_string_steal_c(v, val, len, &irjp_default_memory_fns);
}
RJP_value* rjp_set_string(RJP_value* v, const char* val, RJP_index length){
	return rjp_set_string_c(v, val, length, &irjp_default_memory_fns);
}
RJP_value* rjp_set_object(RJP_value* v){
	return rjp_set_object_c(v, &irjp_default_memory_fns);
}
RJP_value* rjp_set_ordered_object(RJP_value* v){
	return rjp_set_ordered_object_c(v, &irjp_default_memory_fns);
}
RJP_value* rjp_set_array(RJP_value* v){
	return rjp_set_array_c(v, &irjp_default_memory_fns);
}
RJP_value* rjp_set_null_c(RJP_value* v, const RJP_memory_fns* fns){
	irjp_delete_value(v, fns);
	v->type = rjp_json_null;
	return v;
}
RJP_value* rjp_set_int_c(RJP_value* v, RJP_int val, const RJP_memory_fns* fns){
	irjp_delete_value(v, fns);
	v->type = rjp_json_integer;
	v->integer = val;
	return v;
}
RJP_value* rjp_set_float_c(RJP_value* v, RJP_float val, const RJP_memory_fns* fns){
	irjp_delete_value(v, fns);
	v->type = rjp_json_dfloat;
	v->dfloat = val;
	return v;
}
RJP_value* rjp_set_bool_c(RJP_value* v, RJP_bool val, const RJP_memory_fns* fns){
	irjp_delete_value(v, fns);
	v->type = rjp_json_boolean;
	v->boolean = val;
	return v;
}
RJP_value* rjp_set_string_steal_c(RJP_value* v, char* val, RJP_index len, const RJP_memory_fns* fns){
	irjp_delete_value(v, fns);
	v->type = rjp_json_string;
	v->string.value = val;
	v->string.length = len;
	return v;
}
RJP_value* rjp_set_string_c(RJP_value* v, const char* val, RJP_index length, const RJP_memory_fns* fns){
	UNUSED_VARIABLE(length);
	irjp_delete_value(v, fns);
	v->type = rjp_json_string;
	RJP_index esclen = rjp_escape_strlen(val);
	v->string.value = fns->alloc(esclen+1);
	rjp_escape_strcpy(v->string.value, val);
	v->string.value[esclen] = 0;
	v->string.length = esclen;
	return v;
}
RJP_value* rjp_set_object_c(RJP_value* v, const RJP_memory_fns* fns){
	if(v->type == rjp_json_object)
		return v;
	irjp_delete_value(v, fns);
	memset(&v->object, 0, sizeof(RJP_unordered_object));
	v->type = rjp_json_object;
	return v;
}
RJP_value* rjp_set_ordered_object_c(RJP_value* v, const RJP_memory_fns* fns){
	if(v->type == rjp_json_ordered_object)
		return v;
	irjp_delete_value(v, fns);
	memset(&v->orobject, 0, sizeof(RJP_ordered_object));
	v->type = rjp_json_ordered_object;
	return v;
}
RJP_value* rjp_set_array_c(RJP_value* v, const RJP_memory_fns* fns){
	if(v->type == rjp_json_array)
		return v;
	irjp_delete_value(v, fns);
	memset(&v->array, 0, sizeof(RJP_array));
	v->type = rjp_json_array;
	return v;
}

static RJP_index irjp_write_value(char* dest, const RJP_value* val, const int flags, int depth){
		RJP_index ret;
		switch(val->type){
		case rjp_json_integer:
			ret = sprintf(dest, "%" PRId64, val->integer);
			break;
		case rjp_json_dfloat:
			ret = sprintf(dest, "%lf", val->dfloat);
			break;
		case rjp_json_boolean:
			if(val->boolean){
				memcpy(dest, "true", 4);
				ret = 4;
			}else{
				memcpy(dest, "false", 5);
				ret = 5;
			}
			break;
		case rjp_json_null:
			memcpy(dest, "null", 4);
			ret = 4;
			break;
		case rjp_json_string:;
			ret = irjp_copy_string_quoted(dest, val->string.value, val->string.length);
			break;
		case rjp_json_array:
			ret = irjp_dump_array(val, dest, flags, depth);
			break;
		case rjp_json_object:
		case rjp_json_ordered_object:
			ret = irjp_dump_object(val, dest, flags, depth);
			break;
		default:
			ret = 0;
			break;
		};
		return ret;
}

RJP_index irjp_dump_array(const RJP_value* arr, char* dest, const int flags, int depth){
	RJP_index pos = 0;
	RJP_array_iterator it;
	rjp_init_array_iterator(&it, arr);
	RJP_value* current = rjp_array_iterator_current(&it);

	if(!current){
		rjp_delete_array_iterator(&it);
		dest[pos++] = '[';
		dest[pos++] = ']';
		dest[pos] = 0;
		return pos;
	}
	dest[pos++] = '[';
	if(flags & RJP_FORMAT_NEWLINES)
		dest[pos++] = '\n';

	for(;current;current = rjp_array_iterator_next(&it)){
		if(flags & RJP_FORMAT_TABBED_LINES)
			for(int i = 0;i < (depth+1);++i)
				dest[pos++] = '\t';
		pos += irjp_write_value(dest+pos, current, flags, depth+1);

		if(rjp_array_iterator_peek(&it)){
			dest[pos++] = ',';
			if(flags & RJP_FORMAT_COMMA_SPACES)
				dest[pos++] = ' ';
		}
		if(flags & RJP_FORMAT_NEWLINES)
			dest[pos++] = '\n';
	}
	rjp_delete_array_iterator(&it);

	if(flags & RJP_FORMAT_TABBED_LINES)
		for(int i = 0;i < depth;++i)
			dest[pos++] = '\t';
	dest[pos++] = ']';
	dest[pos] = 0;

	return pos;
}

RJP_index irjp_dump_object(const RJP_value* root, char* dest, const int flags, int depth){
	RJP_object_iterator it;
	rjp_init_object_iterator(&it, root);
	RJP_index pos = 0;
	RJP_value* current = rjp_object_iterator_current(&it);
	if(!current){
		rjp_delete_object_iterator(&it);
		dest[pos++] = '{';
		dest[pos++] = '}';
		dest[pos] = 0;
		return pos;
	}
	dest[pos++] = '{';
	if(flags & RJP_FORMAT_NEWLINES)
		dest[pos++] = '\n';

	for(;current;current = rjp_object_iterator_next(&it)){
		const RJP_string* key = rjp_member_key(current);
		if(flags & RJP_FORMAT_TABBED_LINES)
			for(int i = 0;i < (depth+1);++i)
				dest[pos++] = '\t';
		pos += irjp_copy_string_keyed(dest+pos, key->value, key->length);
		if(flags & RJP_FORMAT_KEY_SPACES)
			dest[pos++] = ' ';
		pos += irjp_write_value(dest+pos, current, flags, depth+1);

		if(rjp_object_iterator_peek(&it)){
			dest[pos++] = ',';
			if(flags & RJP_FORMAT_COMMA_SPACES)
				dest[pos++] = ' ';
		}
		if(flags & RJP_FORMAT_NEWLINES)
			dest[pos++] = '\n';
	}
	rjp_delete_object_iterator(&it);

	if(flags & RJP_FORMAT_TABBED_LINES)
		for(int i = 0;i < depth;++i)
			dest[pos++] = '\t';
	dest[pos++] = '}';
	dest[pos] = 0;

	return pos;
}

char* rjp_to_json(const RJP_value* root, const int flags){
	return rjp_to_json_c(root, flags, &irjp_default_memory_fns);
}
char* rjp_to_json_c(const RJP_value* root, const int flags, const RJP_memory_fns* fns){
	if(!root)
		return NULL;
	RJP_index len = irjp_value_strlen(root, flags, 0);
	if(!len)
		return NULL;
	char* tmp = fns->alloc(len + 1);
	tmp[len] = 0;

	irjp_write_value(tmp, root, flags, 0);
	return tmp;
}


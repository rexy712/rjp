/**
	rjp
	Copyright (C) 2018-2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "rjp_internal.h"
#include "rjp_unordered_object.h"
#include "rjp_value.h"
#include "rjp_object_member.h"
#include "tree.h"

#define RJP_TREE_ITERATOR_STACK_START_SIZE 32

typedef struct RJP_tree_stack{
  RJP_tree_node** data;
  int size;
  int pos;
}RJP_tree_stack;

typedef struct RJP_object_iterator_impl{
	RJP_tree_stack stack;
}RJP_object_iterator_impl;

static int irjp_init_tree_stack(RJP_tree_stack* stack);
static void irjp_delete_tree_stack(RJP_tree_stack* stack);
static void irjp_resize_tree_stack(RJP_tree_stack* stack);
static void irjp_push_tree_stack(RJP_tree_stack* stack, RJP_tree_node* value);
static RJP_tree_node* irjp_pop_tree_stack(RJP_tree_stack* stack);
static RJP_tree_node* irjp_peek_tree_stack(RJP_tree_stack* stack);

/* TREE STACK */
//Stack construction / destruction
static int irjp_init_tree_stack(RJP_tree_stack* stack){
	stack->data = rjp_alloc(sizeof(RJP_tree_node*)*RJP_TREE_ITERATOR_STACK_START_SIZE);
	stack->size = RJP_TREE_ITERATOR_STACK_START_SIZE;
	stack->pos = 0;
	return 0;
}
static void irjp_delete_tree_stack(RJP_tree_stack* stack){
	rjp_free(stack->data);
}
static void irjp_resize_tree_stack(RJP_tree_stack* stack){
	int newsize = stack->size*2;
	RJP_tree_node** newdata = rjp_alloc(sizeof(RJP_tree_node*)*newsize);
	for(int i = 0;i < stack->size;++i){
		newdata[i] = stack->data[i];
	}
	rjp_free(stack->data);
	stack->data = newdata;
	stack->size = newsize;
}
//Stack operations
static void irjp_push_tree_stack(RJP_tree_stack* stack, RJP_tree_node* value){
	stack->data[stack->pos++] = value;
	if(stack->pos == stack->size)
		irjp_resize_tree_stack(stack);
}
static RJP_tree_node* irjp_pop_tree_stack(RJP_tree_stack* stack){
	return stack->data[--stack->pos];
}
static RJP_tree_node* irjp_peek_tree_stack(RJP_tree_stack* stack){
	return (stack->pos > 0) ? (stack->data[stack->pos-1]) : NULL;
}
static RJP_tree_node* irjp_double_peek_tree_stack(RJP_tree_stack* stack){
	return (stack->pos > 1) ? (stack->data[stack->pos-2]) : NULL;
}

/* RJP_OBJECT */
void irjp_copy_unordered_object(RJP_value* dest, const RJP_value* src, const RJP_memory_fns* fns){
	dest->object.root = irjp_copy_tree(src->object.root, fns);
}
void irjp_delete_unordered_object(RJP_value* obj, const RJP_memory_fns* fns){
	irjp_free_tree(obj->object.root, fns);
}
RJP_value* irjp_add_unordered_member(RJP_value* dest, char* key, RJP_index keylen, const RJP_memory_fns* fns){
	++dest->object.num_members;
	int status;
	RJP_value* retval;
	dest->object.root = irjp_tree_insert_value(dest->object.root, key, keylen, &retval, &status, fns);
	retval->parent = dest;
	retval->type = rjp_json_null;
	return retval;
}
RJP_value* irjp_remove_unordered_member(RJP_value* obj, RJP_value* member, const RJP_memory_fns* fns){
	RJP_tree_node* removed_node = NULL;
	obj->object.root = irjp_tree_remove_value(obj->object.root, (RJP_tree_node*)member, &removed_node);
	fns->free(((RJP_object_member*)removed_node)->name.value);
	member->parent = NULL;
	--obj->object.num_members;
	return member;
}

void irjp_unordered_set_key(RJP_value* dest, char* key, RJP_index keylen, const RJP_memory_fns* fns){
	RJP_value* parent = dest->parent;
	RJP_tree_node* removed_node = NULL;
	parent->object.root = irjp_tree_remove_value(parent->object.root, (RJP_tree_node*)dest, &removed_node);

	fns->free(((RJP_object_member*)removed_node)->name.value);
	((RJP_object_member*)removed_node)->name.value = key;
	((RJP_object_member*)removed_node)->name.length = keylen;

	parent->object.root = irjp_tree_insert_node(parent->object.root, removed_node);
}
RJP_index irjp_unordered_num_members(const RJP_value* object){
	return object->object.num_members;
}

RJP_value* irjp_unordered_search_member(const RJP_value* object, const char* search){
	RJP_tree_node* n = irjp_tree_search_value(object->object.root, search);
	if(!n)
		return NULL;
	return &n->data.value;
}

void irjp_init_unordered_object_iterator(RJP_object_iterator* it, const RJP_value* object){
	it->it = rjp_alloc(sizeof(RJP_object_iterator_impl));

	RJP_object_iterator_impl* itimpl = it->it;
	RJP_tree_node* root = object ? object->object.root : NULL;
	irjp_init_tree_stack(&itimpl->stack);
	RJP_tree_node* current = (RJP_tree_node*)root;
	while(current){
		irjp_push_tree_stack(&itimpl->stack, current);
		current = current->left;
	}
}
void irjp_delete_unordered_object_iterator(RJP_object_iterator* it){
	if(!it->it)
		return;
	irjp_delete_tree_stack(&it->it->stack);
	rjp_free(it->it);
	it->it = NULL;
}
RJP_value* irjp_unordered_object_iterator_current(const RJP_object_iterator* it){
	RJP_tree_node* n = irjp_peek_tree_stack(&it->it->stack);
	if(!n)
		return NULL;
	return &n->data.value;
}
RJP_value* irjp_unordered_object_iterator_next(RJP_object_iterator* it){
	RJP_object_iterator_impl* itimp = it->it;
	RJP_tree_node* last = irjp_pop_tree_stack(&itimp->stack);
	if(last->right){
		irjp_push_tree_stack(&itimp->stack, last->right);
		last = last->right;
		while(last->left){
			irjp_push_tree_stack(&itimp->stack, last->left);
			last = last->left;
		}
	}
	return irjp_unordered_object_iterator_current(it);
}
RJP_value* irjp_unordered_object_iterator_peek(const RJP_object_iterator* it){
	RJP_object_iterator_impl* itimp = it->it;
	RJP_tree_node* n = irjp_peek_tree_stack(&itimp->stack)->right;
	if(n)
		return &n->data.value;
	n = irjp_double_peek_tree_stack(&itimp->stack);
	if(!n)
		return NULL;
	return &n->data.value;
}


/**
	rjp
	Copyright (C) 2018-2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "rjp_internal.h"
#include "rjp_ordered_object.h"
#include "rjp_object_member.h"
#include "rjp_value.h"
#include "rjp_string.h"

#include <string.h> //strcmp

typedef struct RJP_ordered_member{
	RJP_object_member member;
	struct RJP_ordered_member* prev;
	struct RJP_ordered_member* next;
}RJP_ordered_member;

static void irjp_copy_ordered_member(RJP_ordered_member* dest, const RJP_ordered_member* src, const RJP_memory_fns* fns){
	irjp_strcpy(&dest->member.name, &src->member.name, fns);
	rjp_copy_value_c(&dest->member.value, &src->member.value, fns);
}

void irjp_copy_ordered_object(RJP_value* dest, const RJP_value* src, const RJP_memory_fns* fns){
	irjp_delete_ordered_object(dest, fns);
	dest->orobject.num_members = 0;

	RJP_object_iterator it;
	irjp_init_ordered_object_iterator(&it, src);
	for(const RJP_value* current = irjp_ordered_object_iterator_current(&it);current;current = irjp_ordered_object_iterator_next(&it)){
		RJP_string keycopy;
		irjp_strcpy(&keycopy, &((RJP_object_member*)current)->name, fns);
		RJP_ordered_member* newmemb = (RJP_ordered_member*)irjp_add_ordered_member(dest, keycopy.value, keycopy.length, fns);
		irjp_copy_ordered_member(newmemb, (RJP_ordered_member*)current, fns);
	}
	irjp_delete_ordered_object_iterator(&it);
}
void irjp_delete_ordered_object(RJP_value* obj, const RJP_memory_fns* fns){
	RJP_object_iterator it;
	irjp_init_ordered_object_iterator(&it, obj);
	RJP_value* next = irjp_ordered_object_iterator_current(&it);
	for(RJP_value* current = next;current;current = next){
		next = irjp_ordered_object_iterator_next(&it);
		fns->free(((RJP_object_member*)current)->name.value);
		rjp_free_value_c(current, fns);
	}
	irjp_delete_ordered_object_iterator(&it);
}
static RJP_ordered_member* irjp_append_ordered_member(RJP_ordered_object* dest, RJP_ordered_member* newmemb){
	++dest->num_members;
	if(!dest->members){
		dest->members = dest->last = newmemb;
		newmemb->next = newmemb->prev = NULL;
		return dest->members;
	}
	dest->last->next = newmemb;
	newmemb->prev = dest->last;
	newmemb->next = NULL;
	dest->last = newmemb;
	return newmemb;
}
RJP_value* irjp_add_ordered_member(RJP_value* dest, char* key, RJP_index keylen, const RJP_memory_fns* fns){
	RJP_ordered_member* newmemb = fns->alloc(sizeof(RJP_ordered_member));
	memset(newmemb, 0, sizeof(RJP_ordered_member));
	irjp_append_ordered_member((RJP_ordered_object*)dest, newmemb);
	irjp_ordered_set_key((RJP_value*)newmemb, key, keylen, fns);
	((RJP_value*)newmemb)->parent = dest;
	((RJP_value*)newmemb)->type = rjp_json_null;
	return (RJP_value*)newmemb;
}
RJP_value* irjp_remove_ordered_member(RJP_value* obj, RJP_value* member, const RJP_memory_fns* fns){
	RJP_ordered_member* current = (RJP_ordered_member*)member;
	RJP_ordered_member* prev = current->prev;
	RJP_ordered_member* next = current->next;

	--obj->orobject.num_members;
	if(prev)
		prev->next = next;
	else
		obj->orobject.members = next;
	if(next)
		next->prev = prev;
	else
		obj->orobject.last = prev;

	member->parent = NULL;
	fns->free(((RJP_object_member*)member)->name.value);
	return member;
}
void irjp_ordered_set_key(RJP_value* dest, char* key, RJP_index keylen, const RJP_memory_fns* fns){
	RJP_object_member* mem = (RJP_object_member*)dest;
	fns->free(mem->name.value);
	mem->name.value = key;
	mem->name.length = keylen;
}
RJP_index irjp_ordered_num_members(const RJP_value* object){
	return object->orobject.num_members;
}
RJP_value* irjp_ordered_search_member(const RJP_value* object, const char* search){
	RJP_value* retval = NULL;
	RJP_object_iterator it;
	irjp_init_ordered_object_iterator(&it, object);
	for(RJP_value* current = irjp_ordered_object_iterator_current(&it);current;current = irjp_ordered_object_iterator_next(&it)){
		if(!strcmp(((RJP_object_member*)current)->name.value, search)){
			retval = current;
			break;
		}
	}
	irjp_delete_ordered_object_iterator(&it);
	return retval;
}


void irjp_init_ordered_object_iterator(RJP_object_iterator* it, const RJP_value* object){
	it->current = &(object->orobject.members->member.value);
	it->type = rjp_json_ordered_object;
}
void irjp_delete_ordered_object_iterator(RJP_object_iterator* it){
	it->current = NULL;
}
RJP_value* irjp_ordered_object_iterator_current(const RJP_object_iterator* it){
	return it->current;
}
RJP_value* irjp_ordered_object_iterator_next(RJP_object_iterator* it){
	RJP_ordered_member* curr = (RJP_ordered_member*)it->current;
	it->current = curr ? &(curr->next->member.value) : NULL;
	return it->current;
}
RJP_value* irjp_ordered_object_iterator_peek(const RJP_object_iterator* it){
	RJP_ordered_member* curr = (RJP_ordered_member*)it->current;
	return curr ? &(curr->next->member.value) : NULL;
}

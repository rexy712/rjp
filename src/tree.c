/**
	rjp
	Copyright (C) 2018-2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "tree.h"

#include "rjp_string.h"
#include "rjp_internal.h"
#include "rjp_value.h"

#include <string.h> //memset, strcmp

#define BLACK 0
#define RED   1


//Forward declare static functions
static inline RJP_tree_node* irjp_grandparent(RJP_tree_node* n);
static inline RJP_tree_node* irjp_sibling(RJP_tree_node* n);
static inline RJP_tree_node* irjp_uncle(RJP_tree_node* n);
static inline RJP_tree_node* irjp_rotate_left(RJP_tree_node* pivot);
static inline RJP_tree_node* irjp_rotate_right(RJP_tree_node* pivot);
static inline int irjp_is_inside_left(RJP_tree_node* n);
static inline int irjp_is_inside_right(RJP_tree_node* n);
static void irjp_replace_node(RJP_tree_node *restrict node, RJP_tree_node *restrict child);

static void irjp_copy_node_data(RJP_tree_node *restrict dest, RJP_tree_node *restrict src);
static RJP_tree_node* irjp_clone_node(const RJP_tree_node* src, RJP_tree_node* parent, const RJP_memory_fns* fns);
static RJP_tree_node* irjp_copy_node(const RJP_tree_node* root, RJP_tree_node* parent, const RJP_memory_fns* fns);
static void irjp_free_node(RJP_tree_node* node, const RJP_memory_fns* fns);
static void irjp_delete_node(RJP_tree_node* node, const RJP_memory_fns* fns);

static RJP_tree_node* irjp_tree_insert_impl(RJP_tree_node *restrict root, RJP_tree_node *restrict newnode);
static RJP_tree_node* irjp_tree_repair(RJP_tree_node* node);
static RJP_tree_node* irjp_tree_remove_node(RJP_tree_node* target, RJP_tree_node** removed_node);


//Tree helpers
static inline RJP_tree_node* irjp_grandparent(RJP_tree_node* n){
	if(!n->parent)
		return NULL;
	return n->parent->parent;
}
static inline RJP_tree_node* irjp_sibling(RJP_tree_node* n){
	RJP_tree_node* p = n->parent;
	if(!p)
		return NULL;
	if(p->left == n)
		return p->right;
	return p->left;
}
static inline RJP_tree_node* irjp_uncle(RJP_tree_node* n){
	if(!n->parent)
		return NULL;
	return irjp_sibling(n->parent);
}
static inline RJP_tree_node* irjp_rotate_right(RJP_tree_node* pivot){
	RJP_tree_node* newroot = pivot->left;
	pivot->left = newroot->right;
	newroot->right = pivot;
	newroot->parent = pivot->parent;
	pivot->parent = newroot;
	if(newroot->parent){
		if(newroot->parent->left == pivot)
			newroot->parent->left = newroot;
		else
			newroot->parent->right = newroot;
	}
	if(pivot->left)
		pivot->left->parent = pivot;
	return newroot;
}
static inline RJP_tree_node* irjp_rotate_left(RJP_tree_node* pivot){
	RJP_tree_node* newroot = pivot->right;
	pivot->right = newroot->left;
	newroot->left = pivot;
	newroot->parent = pivot->parent;
	pivot->parent = newroot;
	if(newroot->parent){
		if(newroot->parent->left == pivot)
			newroot->parent->left = newroot;
		else
			newroot->parent->right = newroot;
	}
	if(pivot->right)
		pivot->right->parent = pivot;
	return newroot;
}
static inline int irjp_is_inside_left(RJP_tree_node* n){
	RJP_tree_node* p = n->parent;
	RJP_tree_node* g = irjp_grandparent(n);
	return n == p->right && p == g->left;
}
static inline int irjp_is_inside_right(RJP_tree_node* n){
	RJP_tree_node* p = n->parent;
	RJP_tree_node* g = irjp_grandparent(n);
	return n == p->left && p == g->right;
}
static void irjp_replace_node(RJP_tree_node *restrict node, RJP_tree_node *restrict child){
	if(child)
		child->parent = node->parent;
	if(!node->parent)
		return;
	if(node == node->parent->left)
		node->parent->left = child;
	else
		node->parent->right = child;
}

//Node construction / destruction
RJP_tree_node* irjp_new_node(char* key, RJP_index keylen, const RJP_memory_fns* fns){
	RJP_tree_node* node = fns->alloc(sizeof(RJP_tree_node));
	memset(node, 0, sizeof(RJP_tree_node));
	node->data.name.value = key;
	node->data.name.length = keylen;
	node->parent = node->left = node->right = NULL;
	node->color = BLACK;
	return node;
}
static void irjp_copy_node_data(RJP_tree_node *restrict dest, RJP_tree_node *restrict src){
	dest->data = src->data;
}
static RJP_tree_node* irjp_clone_node(const RJP_tree_node* src, RJP_tree_node* parent, const RJP_memory_fns* fns){
	RJP_tree_node* dest = fns->alloc(sizeof(RJP_tree_node));
	memset(dest, 0, sizeof(RJP_tree_node));
	rjp_copy_value_c(&dest->data.value, &src->data.value, fns);
	irjp_strcpy(&dest->data.name, &src->data.name, fns);
	dest->parent = parent;
	return dest;
}
static RJP_tree_node* irjp_copy_node(const RJP_tree_node* root, RJP_tree_node* parent, const RJP_memory_fns* fns){
	if(!root){
		return NULL;
	}
	RJP_tree_node* newnode = irjp_clone_node(root, parent, fns);
	newnode->left = irjp_copy_node(root->left, newnode, fns);
	newnode->right = irjp_copy_node(root->right, newnode, fns);
	return newnode;
}
static void irjp_free_node(RJP_tree_node* node, const RJP_memory_fns* fns){
	irjp_delete_node(node, fns);
	fns->free(node);
}
static void irjp_delete_node(RJP_tree_node* node, const RJP_memory_fns* fns){
	fns->free(node->data.name.value);
	irjp_delete_value(&node->data.value, fns);
}


/* TREE */
//Tree construction / destruction
RJP_tree_node* irjp_copy_tree(const RJP_tree_node* root, const RJP_memory_fns* fns){
	return irjp_copy_node(root, NULL, fns);
}
void irjp_free_tree(RJP_tree_node* root, const RJP_memory_fns* fns){
	if(!root)
		return;
	irjp_free_tree(root->left, fns);
	irjp_free_tree(root->right, fns);
	irjp_free_node(root, fns);
}
//Tree operations
RJP_tree_node* irjp_tree_insert_value(RJP_tree_node* root, char* key, RJP_index keylen, RJP_value** added, int* status, const RJP_memory_fns* fns){
	*status = RJP_TREE_SUCCESS;
	if(!root){
		root = irjp_new_node(key, keylen, fns);
		if(added)
			*added = &root->data.value;
		return root;
	}
	RJP_tree_node* newnode = irjp_new_node(key, keylen, fns);
	if(added)
		*added = &newnode->data.value;
	return irjp_tree_insert_node(root, newnode);
}
RJP_tree_node* irjp_tree_insert_node(RJP_tree_node *restrict root, RJP_tree_node *restrict newnode){
	irjp_tree_insert_impl(root, newnode);
	irjp_tree_repair(newnode);
	while(root->parent)
		root = root->parent;
	return root;
}
static RJP_tree_node* irjp_tree_insert_impl(RJP_tree_node *restrict root, RJP_tree_node *restrict newnode){
	if(!root){
		return newnode;
	}
	RJP_tree_node* n = root;
	while(1){
		//compare json key strings
		int cmpval = strcmp(newnode->data.name.value, n->data.name.value);
		if(cmpval < 0){
			if(n->left){
				n = n->left;
			}else{
				n->left = newnode;
				newnode->parent = n;
				break;
			}
		}else{
			if(n->right){
				n = n->right;
			}else{
				n->right = newnode;
				newnode->parent = n;
				break;
			}
		}
	}
	return newnode;
}
static RJP_tree_node* irjp_tree_repair(RJP_tree_node* node){
	node->color = RED;
	while(1){
		if(!node->parent){
			node->color = BLACK;
			return node;
		}else if(node->parent->color == BLACK){
			return node;
		}else if(irjp_uncle(node) && irjp_uncle(node)->color == RED){
			node->parent->color = BLACK;
			irjp_uncle(node)->color = BLACK;
			node = irjp_grandparent(node);
		}else{
			if(irjp_is_inside_left(node)){
				irjp_rotate_left(node->parent);
				node = node->left;
			}else if(irjp_is_inside_right(node)){
				irjp_rotate_right(node->parent);
				node = node->right;
			}
			RJP_tree_node* pa = node->parent;
			RJP_tree_node* gp = irjp_grandparent(node);
			if(node == pa->left){
				irjp_rotate_right(gp);
			}else{
				irjp_rotate_left(gp);
			}
			pa->color = BLACK;
			gp->color = RED;
			return node;
		}
	}
}

RJP_tree_node* irjp_tree_search_value(RJP_tree_node* root, const char* key){
	while(root != NULL){
		int cmpval = strcmp(key, root->data.name.value);
		if(cmpval < 0){
			root = root->left;
		}else if(cmpval > 0){
			root = root->right;
		}else{
			return root;
		}
	}
	return NULL;
}
RJP_tree_node* irjp_tree_remove_value(RJP_tree_node* restrict root, RJP_tree_node* restrict member, RJP_tree_node** removed_node){
	if(!root)
		return root;
	if(!member)
		return root;
	return irjp_tree_remove_node(member, removed_node);
}
static RJP_tree_node* irjp_tree_remove_node(RJP_tree_node* target, RJP_tree_node** removed_node){
	while(target->right && target->left){
		irjp_copy_node_data(target, target->right);
		target = target->right;
	}
	*removed_node = target;
	RJP_tree_node* retval = target->parent;

	do{
		RJP_tree_node* child = (target->left) ? target->left : target->right;
		//handle trivial cases
		if(target->color == RED){
			irjp_replace_node(target, child);
			target->parent = NULL;
			break;
		}else{
			if(child && child->color == RED){
				child->color = BLACK;
			}
			irjp_replace_node(target, child);
			if(!retval)
				retval = child;
			break;
		}
		if(!target->parent){
			retval = child;
			break;

		//handle harder cases
		}else{
			//parent and sibling guaranteed to exist at this point
			RJP_tree_node* sibling = irjp_sibling(target);

			//Deal with red sibling
			if(sibling->color == RED){
				sibling->color = BLACK;
				target->parent->color = RED;
				if(target == target->parent->left){
					irjp_rotate_left(target->parent);
				}else{
					irjp_rotate_right(target->parent);
				}
				sibling = irjp_sibling(target);
			}

			//sibling guaranteed to be black here
			if(target->parent->color == BLACK &&
			   ((!sibling->left) || sibling->left->color == BLACK) &&
			   ((!sibling->right) || sibling->right->color == BLACK))
			{
				sibling->color = RED;
				target = target->parent;
				continue;
			}

			//sibling guaranteed to be black here
			if(target->parent->color == RED &&
			   ((!sibling->left) || sibling->left->color == BLACK) &&
			   ((!sibling->right) || sibling->right->color == BLACK))
			{
				sibling->color = RED;
				target->parent->color = BLACK;
				break;
			}

			//Orient the subtree for the following section
			if((target == target->parent->left) && (!sibling->right || sibling->right->color == BLACK) &&
			   (sibling->left && sibling->left->color == RED))
			{
				sibling->color = RED;
				if(sibling->left)
					sibling->left->color = BLACK;
				irjp_rotate_right(sibling);
				sibling = irjp_sibling(target);
			}else if((target == target->parent->right) && (sibling->right && sibling->right->color == RED) &&
			         (!sibling->left || sibling->left->color == BLACK))
			{
				sibling->color = RED;
				if(sibling->right)
					sibling->right->color = BLACK;
				irjp_rotate_left(sibling);
				sibling = irjp_sibling(target);
			}

			sibling->color = target->parent->color;
			target->parent->color = BLACK;
			if(target == target->parent->left){
				//guaranteed to not be NULL
				sibling->right->color = BLACK;
				irjp_rotate_left(target->parent);
			}else{
				//guaranteed to not be NULL
				sibling->left->color = BLACK;
				irjp_rotate_right(target->parent);
			}
			break;
		}
	}while(1);

	(*removed_node)->left = NULL;
	(*removed_node)->right = NULL;
	//return new root
	if(!retval)
		return NULL;
	while(retval->parent)
		retval = retval->parent;
	return retval;
}


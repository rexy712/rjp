/**
	rjp
	Copyright (C) 2018-2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "rjp_internal.h"
#include "rjp_string.h"
#include "rjp_object.h"
#include "rjp_array.h"
#include "rjp_value.h"

#include <stdlib.h> //free, malloc, calloc

void* rjp_alloc(RJP_index nbytes){
	return malloc(nbytes);
}
void* rjp_calloc(RJP_index num, RJP_index nbytes){
	return calloc(num, nbytes);
}
void rjp_free(void* data){
	free(data);
}

const RJP_memory_fns irjp_default_memory_fns = {rjp_free, rjp_alloc};

RJP_value* rjp_copy_value(RJP_value* dest, const RJP_value* src){
	return rjp_copy_value_c(dest, src, &irjp_default_memory_fns);
}
RJP_value* rjp_copy_value_c(RJP_value* dest, const RJP_value* src, const RJP_memory_fns* fns){
	RJP_value* ret;
	if(dest){
		irjp_delete_value(dest, fns);
		ret = dest;
	}else{
		ret = rjp_new_null_c(fns);
	}
	ret->type = src->type;

	switch(src->type){
	case rjp_json_object:
		irjp_copy_object(ret, src, fns);
		break;
	case rjp_json_ordered_object:
		irjp_copy_ordered_object(ret, src, fns);
		break;
	case rjp_json_array:
		irjp_copy_array(ret, src, fns);
		break;
	case rjp_json_string:
		irjp_strcpy(&ret->string, &src->string, fns);
		break;
	case rjp_json_integer:
		ret->integer = src->integer;
		break;
	case rjp_json_boolean:
		ret->boolean = src->boolean;
		break;
	case rjp_json_dfloat:
		ret->dfloat = src->dfloat;
		break;
	default:
		break;
	};
	return ret;
}
RJP_value* rjp_move_value(RJP_value* dest, RJP_value* src){
	*dest = *src;
	src->type = rjp_json_null;
	return dest;
}

static void irjp_delete_array(RJP_value* root, const RJP_memory_fns* fns){
	RJP_array_iterator it;
	rjp_init_array_iterator(&it, root);
	RJP_value* next = rjp_array_iterator_current(&it);

	for(RJP_value* current = next;current;current = next){
		next = rjp_array_iterator_next(&it);
		rjp_free_value_c(current, fns);
	}
}

void irjp_delete_value(RJP_value* root, const RJP_memory_fns* fns){
	switch(root->type){
	case rjp_json_object:
	case rjp_json_ordered_object:
		irjp_delete_object(root, fns);
		break;
	case rjp_json_array:
		irjp_delete_array(root, fns);
		break;
	case rjp_json_string:
		fns->free(root->string.value);
		break;
	default: break;
	};
}
void rjp_free_value(RJP_value* root){
	rjp_free_value_c(root, &irjp_default_memory_fns);
}
void rjp_free_value_c(RJP_value* root, const RJP_memory_fns* fns){
	if(!root)
		return;
	irjp_delete_value(root, fns);
	fns->free(root);
}

/* VALUE SETTING */
/* GETTERS */
RJP_value* rjp_value_parent(const RJP_value* value){
	return value->parent;
}
RJP_data_type rjp_value_type(const RJP_value* value){
	return value->type;
}
RJP_float rjp_get_float(const RJP_value* value){
	return value->dfloat;
}
RJP_int rjp_get_int(const RJP_value* value){
	return value->integer;
}
RJP_bool rjp_get_bool(const RJP_value* value){
	return value->boolean;
}
RJP_string* rjp_get_string(RJP_value* value){
	return &(value->string);
}
const RJP_string* rjp_get_cstring(const RJP_value* value){
	return &(value->string);
}

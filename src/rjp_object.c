/**
	rjp
	Copyright (C) 2018-2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "rjp_internal.h"
#include "tree.h"
#include "rjp_object.h"
#include "rjp_object_member.h"
#include "rjp_value.h"

#include <string.h> //strlen, strncpy

void irjp_copy_object(RJP_value* dest, const RJP_value* src, const RJP_memory_fns* fns){
	switch(src->type){
	case rjp_json_object:
		irjp_copy_unordered_object(dest, src, fns);
		break;
	case rjp_json_ordered_object:
		irjp_copy_ordered_object(dest, src, fns);
		break;
	default: break;
	};
}
void irjp_delete_object(RJP_value* obj, const RJP_memory_fns* fns){
	switch(obj->type){
	case rjp_json_object:
		irjp_delete_unordered_object(obj, fns);
		break;
	case rjp_json_ordered_object:
		irjp_delete_ordered_object(obj, fns);
		break;
	default: break;
	};
}
RJP_value* rjp_new_member_steal_key(RJP_value* dest, char* key, RJP_index keylen){
	return rjp_new_member_steal_key_c(dest, key, keylen, &irjp_default_memory_fns);
}
RJP_value* rjp_new_member_steal_key_c(RJP_value* dest, char* key, RJP_index keylen, const RJP_memory_fns* fns){
	if(!key)
		return NULL;
	if(!keylen)
		if(!(keylen = strlen(key)))
			return NULL;
	switch(dest->type){
	case rjp_json_object:
		return irjp_add_unordered_member(dest, key, keylen, fns);
	case rjp_json_ordered_object:
		return irjp_add_ordered_member(dest, key, keylen, fns);
	default: break;
	};
	return NULL;
}
RJP_value* rjp_new_member(RJP_value* dest, const char* key, RJP_index keylen){
	return rjp_new_member_c(dest, key, keylen, &irjp_default_memory_fns);
}
RJP_value* rjp_new_member_c(RJP_value* dest, const char* key, RJP_index keylen, const RJP_memory_fns* fns){
	if(!key)
		return NULL;
	if(!keylen)
		if(!(keylen = rjp_escape_strlen(key)))
			return NULL;
	char* newkey = fns->alloc(keylen+1);
	rjp_escape_strcpy(newkey, key);
	newkey[keylen] = 0;
	return rjp_new_member_steal_key_c(dest, newkey, keylen, fns);
}
RJP_value* rjp_add_member_steal_key(RJP_value* dest, char* key, RJP_index keylen, RJP_value* src){
	return rjp_add_member_steal_key_c(dest, key, keylen, src, &irjp_default_memory_fns);
}
RJP_value* rjp_add_member_steal_key_c(RJP_value* dest, char* key, RJP_index keylen, RJP_value* src, const RJP_memory_fns* fns){
	RJP_value* newval = rjp_new_member_steal_key_c(dest, key, keylen, fns);
	if(!newval)
		return NULL;
	rjp_move_value(newval, src);
	fns->free(src);
	return newval;
}
RJP_value* rjp_add_member(RJP_value* dest, const char* key, RJP_index keylen, RJP_value* src){
	return rjp_add_member_c(dest, key, keylen, src, &irjp_default_memory_fns);
}
RJP_value* rjp_add_member_c(RJP_value* dest, const char* key, RJP_index keylen, RJP_value* src, const RJP_memory_fns* fns){
	RJP_value* newval = rjp_new_member_c(dest, key, keylen, fns);
	if(!newval)
		return NULL;
	rjp_move_value(newval, src);
	fns->free(src);
	return newval;
}
RJP_value* rjp_remove_member_by_key(RJP_value* obj, const char* key){
	return rjp_remove_member_by_key_c(obj, key, &irjp_default_memory_fns);
}
RJP_value* rjp_remove_member_by_key_c(RJP_value* obj, const char* key, const RJP_memory_fns* fns){
	RJP_value* member = rjp_search_member(obj, key);
	if(!member)
		return NULL;
	return rjp_remove_member_c(obj, member, fns);
}
RJP_value* rjp_remove_member(RJP_value* obj, RJP_value* member){
	return rjp_remove_member_c(obj, member, &irjp_default_memory_fns);
}
RJP_value* rjp_remove_member_c(RJP_value* obj, RJP_value* member, const RJP_memory_fns* fns){
	if(member->parent != obj)
		return NULL;
	switch(obj->type){
	case rjp_json_object:
		return irjp_remove_unordered_member(obj, member, fns);
	case rjp_json_ordered_object:
		return irjp_remove_ordered_member(obj, member, fns);
	default: break;
	};
	return NULL;
}
void rjp_free_member_by_key(RJP_value* obj, const char* key){
	rjp_free_member_by_key_c(obj, key, &irjp_default_memory_fns);
}
void rjp_free_member_by_key_c(RJP_value* obj, const char* key, const RJP_memory_fns* fns){
	RJP_value* removed = rjp_remove_member_by_key(obj, key);
	rjp_free_value_c(removed, fns);
}
void rjp_free_member(RJP_value* obj, RJP_value* member){
	rjp_free_member_c(obj, member, &irjp_default_memory_fns);
}
void rjp_free_member_c(RJP_value* obj, RJP_value* member, const RJP_memory_fns* fns){
	rjp_free_member_by_key_c(obj, ((RJP_object_member*)member)->name.value, fns);
}

RJP_value* rjp_set_key(RJP_value* dest, const char* key, RJP_index keylen){
	return rjp_set_key_c(dest, key, keylen, &irjp_default_memory_fns);
}
RJP_value* rjp_set_key_c(RJP_value* dest, const char* key, RJP_index keylen, const RJP_memory_fns* fns){
	if(!key)
		return NULL;
	if(!keylen){
		if(!(keylen = strlen(key)))
			return NULL;
	}
	char* newkey = fns->alloc(keylen + 1);
	strncpy(newkey, key, keylen+1);
	return rjp_set_key_steal(dest, newkey, keylen);
}
RJP_value* rjp_set_key_steal(RJP_value* dest, char* key, RJP_index keylen){
	return rjp_set_key_steal_c(dest, key, keylen, &irjp_default_memory_fns);
}
RJP_value* rjp_set_key_steal_c(RJP_value* dest, char* key, RJP_index keylen, const RJP_memory_fns* fns){
	if(!key)
		return NULL;
	if(!keylen){
		if(!(keylen = strlen(key)))
			return NULL;
	}
	switch(dest->parent->type){
	case rjp_json_object:
		irjp_unordered_set_key(dest, key, keylen, fns);
		break;
	case rjp_json_ordered_object:
		irjp_ordered_set_key(dest, key, keylen, fns);
		break;
	default: break;
	};
	return dest;
}
RJP_index rjp_num_members(const RJP_value* object){
	switch(object->type){
	case rjp_json_object:
		return irjp_unordered_num_members(object);
	case rjp_json_ordered_object:
		return irjp_ordered_num_members(object);
	default: break;
	};
	return 0;
}
const RJP_string* rjp_member_key(const RJP_value* member){
	return &(((RJP_object_member*)member)->name);
}

RJP_value* rjp_search_member(const RJP_value* object, const char* search){
	if(!object)
		return NULL;
	switch(object->type){
	case rjp_json_object:
		return irjp_unordered_search_member(object, search);
	case rjp_json_ordered_object:
		return irjp_ordered_search_member(object, search);
	default: break;
	};
	return NULL;
}

void rjp_init_object_iterator(RJP_object_iterator* it, const RJP_value* object){
	switch(object->type){
	case rjp_json_object:
		irjp_init_unordered_object_iterator(it, object);
		it->type = rjp_json_object;
		break;
	case rjp_json_ordered_object:
		irjp_init_ordered_object_iterator(it, object);
		it->type = rjp_json_ordered_object;
		break;
	default: break;
	};
}
void rjp_delete_object_iterator(RJP_object_iterator* it){
	switch(it->type){
	case rjp_json_object:
		irjp_delete_unordered_object_iterator(it);
		break;
	case rjp_json_ordered_object:
		irjp_delete_ordered_object_iterator(it);
		break;
	default: break;
	};
}
RJP_value* rjp_object_iterator_current(const RJP_object_iterator* it){
	switch(it->type){
	case rjp_json_object:
		return irjp_unordered_object_iterator_current(it);
		break;
	case rjp_json_ordered_object:
		return irjp_ordered_object_iterator_current(it);
		break;
	default: break;
	};
	return NULL;
}
RJP_value* rjp_object_iterator_next(RJP_object_iterator* it){
	switch(it->type){
	case rjp_json_object:
		return irjp_unordered_object_iterator_next(it);
		break;
	case rjp_json_ordered_object:
		return irjp_ordered_object_iterator_next(it);
		break;
	default: break;
	};
	return NULL;
}
RJP_value* rjp_object_iterator_peek(const RJP_object_iterator* it){
	switch(it->type){
	case rjp_json_object:
		return irjp_unordered_object_iterator_peek(it);
		break;
	case rjp_json_ordered_object:
		return irjp_ordered_object_iterator_peek(it);
		break;
	default: break;
	};
	return NULL;
}

RJP_value* rjp_object_to_ordered(RJP_value* object){
	return rjp_object_to_ordered_c(object, &irjp_default_memory_fns);
}
RJP_value* rjp_object_to_ordered_c(RJP_value* object, const RJP_memory_fns* fns){
	if(object->type == rjp_json_ordered_object)
		return object;
	RJP_value newobj = {0};
	newobj.type = rjp_json_ordered_object;

	RJP_object_iterator it;
	irjp_init_unordered_object_iterator(&it, object);
	RJP_value* next = irjp_unordered_object_iterator_current(&it);
	for(RJP_value* current = next;current;current = next){
		RJP_value* newcur = irjp_add_ordered_member(&newobj, rjp_member_key(current)->value, rjp_member_key(current)->length, fns);
		((RJP_object_member*)current)->name.value = NULL;
		rjp_move_value(newcur, current);
		next = irjp_unordered_object_iterator_next(&it);
		fns->free(current);
	}
	irjp_delete_unordered_object_iterator(&it);

	object->type = rjp_json_ordered_object;
	object->orobject = newobj.orobject;

	return object;
}
RJP_value* rjp_object_to_unordered(RJP_value* object){
	return rjp_object_to_unordered_c(object, &irjp_default_memory_fns);
}
RJP_value* rjp_object_to_unordered_c(RJP_value* object, const RJP_memory_fns* fns){
	if(object->type == rjp_json_object)
		return object;
	RJP_value newobj = {0};
	newobj.type = rjp_json_object;

	RJP_object_iterator it;
	irjp_init_ordered_object_iterator(&it, object);
	RJP_value* next = irjp_ordered_object_iterator_current(&it);
	for(RJP_value* current = next;current;current = next){
		RJP_value* newcur = irjp_add_unordered_member(&newobj, rjp_member_key(current)->value, rjp_member_key(current)->length, fns);
		((RJP_object_member*)current)->name.value = NULL;
		rjp_move_value(newcur, current);
		next = irjp_ordered_object_iterator_next(&it);
		fns->free(current);
	}
	irjp_delete_ordered_object_iterator(&it);

	object->type = rjp_json_object;
	object->object = newobj.object;

	return object;
}

/**
	rjp++
	Copyright (C) 2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RJP_VALUE_HPP
#define RJP_VALUE_HPP

#include <rjp.h>
#include "string.hpp"

namespace rjp{

	class value;

	namespace detail{
		class vget_proxy
		{
		private:
			const rjp::value* m_value;
		public:
			vget_proxy(const rjp::value* v);
			operator int(void)const;
			operator bool(void)const;
			operator rexy::string_view(void)const;
			operator double(void)const;
		};
	}

	class value
	{
	protected:
		RJP_value* m_value;
		bool m_managed = true;

	public:
		value(void);
		explicit value(RJP_value* val, bool managed);
		value(const value& val);
		value(value&& val);
		~value(void);

		value& operator=(const value& val);
		value& operator=(value&& val);

		operator bool(void)const;
		bool valid(void)const;

		const RJP_value* raw(void)const;
		RJP_value* raw(void);

		RJP_data_type type(void)const;

		detail::vget_proxy get(void)const;

		value parent(void)const;
		bool is_child_of(const value&)const;

		string to_json(int flags = RJP_FORMAT_PRETTY);
	protected:
		static value create_unmanaged(RJP_value* val);
		static value create_managed(RJP_value* val);
		static void remove_management(value& val);
		static void add_management(value& val);
	};

}

#endif

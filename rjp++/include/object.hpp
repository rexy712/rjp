/**
	rjp++
	Copyright (C) 2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RJP_OBJECT_HPP
#define RJP_OBJECT_HPP

#include <rjp.h>
#include <rexy/string_base.hpp>
#include <cstdlib> //size_t
#include "value.hpp"
#include "iterator.hpp"
#include "member.hpp"
#include "integral.hpp"
#include "string_val.hpp"
#include "rjp_util.hpp"
#include "container.hpp"

namespace rjp{

	class array;

	class object_iterator : protected detail::iterator_base
	{
	protected:
		RJP_object_iterator m_it;

	public:
		object_iterator(void);
		object_iterator(RJP_value* v);
		object_iterator(const object_iterator&) = delete;
		object_iterator(object_iterator&& o);
		~object_iterator(void);
		object_iterator& operator=(const object_iterator&) = delete;
		object_iterator& operator=(object_iterator&& o);

		bool operator==(const object_iterator& o)const;
		bool operator!=(const object_iterator& o)const;
		member<void> operator*(void)const;
		value_wrapper operator->(void)const;

		object_iterator& operator++(void);
	};

	class object : public value, private container
	{
	public:
		using iterator = object_iterator;
		using const_iterator = const iterator;
		using size_type = size_t;
	public:
		using value::value;
		object(void);
		object(const value& val);
		object(value&& val);

		object(const object&) = default;
		object(object&&) = default;
		~object(void) = default;
		object& operator=(const object&) = default;
		object& operator=(object&&) = default;

		template<class Val>
		member<std::decay_t<Val>> add(rexy::string_view key, typename std::decay_t<Val>::underlying_type t){
			RJP_value* newmem = add_member_impl(key);
			detail::set_to_underlying<std::decay_t<Val>>(newmem, t);
			return create_unmanaged(newmem);
		}
		template<class Val = null>
		member<std::decay_t<Val>> add(rexy::string_view key){
			RJP_value* newmem = add_member_impl(key);
			if constexpr(std::is_same<std::decay_t<Val>,rjp::array>::value)
				set_array_value(newmem);
			else if constexpr(std::is_same<std::decay_t<Val>,rjp::object>::value)
				set_object_value(newmem);
			else if constexpr(std::is_void<typename std::decay_t<Val>::underlying_type>::value)
				set_null_value(newmem);
			else
				detail::set_to_underlying<std::decay_t<Val>>(newmem, 0);
			return create_unmanaged(newmem);
		}
		member<string_val> add(rexy::string_view key, const RJP_string&);
		member<string_val> add(rexy::string_view key, RJP_string&&);
		member<string_val> add(rexy::string_view key, const char*, RJP_index len = 0);
		member<string_val> add(rexy::string_view key, rexy::string_view);
		member<string_val> add(rexy::string_view key, string&&);

		template<class Val>
		member<std::decay_t<Val>> add(string&& key, typename std::decay_t<Val>::underlying_type t){
			RJP_value* newmem = add_member_impl(std::move(key));
			detail::set_to_underlying<std::decay_t<Val>>(newmem, t);
			return create_unmanaged(newmem);
		}
		template<class Val = null>
		member<std::decay_t<Val>> add(string&& key){
			RJP_value* newmem = add_member_impl(std::move(key));
			if constexpr(std::is_same<std::decay_t<Val>,rjp::array>::value)
				set_array_value(newmem);
			else if constexpr(std::is_same<std::decay_t<Val>,rjp::object>::value)
				set_object_value(newmem);
			else if constexpr(std::is_void<typename std::decay_t<Val>::underlying_type>::value)
				set_null_value(newmem);
			else
				detail::set_to_underlying<std::decay_t<Val>>(newmem, 0);
			return create_unmanaged(newmem);
		}
		member<string_val> add(string&& key, const RJP_string&);
		member<string_val> add(string&& key, RJP_string&&);
		member<string_val> add(string&& key, const char*, RJP_index len = 0);
		member<string_val> add(string&& key, rexy::string_view);
		member<string_val> add(string&& key, string&&);

		value remove(rexy::string_view key);
		value& remove(value& val);

		void destroy(rexy::string_view key);
		void destroy(value&& val);

		bool has_child(const value&);

		iterator begin(void);
		const_iterator begin(void)const;
		iterator end(void);
		const_iterator end(void)const;

		const_iterator cbegin(void)const;
		const_iterator cend(void)const;

		size_type size(void)const;

		value search(rexy::string_view key)const;

	private:
		RJP_value* add_member_impl(rexy::string_view key);
		RJP_value* add_member_impl(string&& key);
	};

}

#endif

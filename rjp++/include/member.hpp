/**
	rjp++
	Copyright (C) 2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RJP_MEMBER_HPP
#define RJP_MEMBER_HPP

#include "value.hpp"
#include "string.hpp"
#include <rexy/string.hpp>

#include <utility> //move

namespace rjp{

	struct member_base
	{
		static const RJP_string* member_key(RJP_value* v);
	};
	template<class Type>
	class member : public Type, private member_base
	{
	public:
		member(void) = default;
		member(const member&) = default;
		member(member&&) = default;
		member(const Type& val):
			Type(val){}
		member(Type&& val):
			Type(std::move(val)){}
		member(const value& val):
			Type(val){}
		member(value&& val):
			Type(std::move(val)){}
		using Type::Type;
		~member(void) = default;

		member& operator=(const member&) = default;
		member& operator=(member&&) = default;

		rexy::string_view key(void)const{
			return rexy::string_view(member_key(this->m_value)->value, member_key(this->m_value)->length);
		}
		string steal_key(void){
			return string(this->m_value);
		}
	};
	template<>
	class member<void> : public value, private member_base
	{
	public:
		member(void) = default;
		member(const member&) = default;
		member(member&&) = default;
		member(const value& val):
			value(val){}
		member(value&& val):
			value(std::move(val)){}
		using value::value;
		~member(void) = default;

		member& operator=(const member&) = default;
		member& operator=(member&&) = default;

		rexy::string_view key(void)const{
			return rexy::string_view(member_key(m_value)->value, member_key(m_value)->length);
		}
		string steal_key(void){
			return string(m_value);
		}
	};

}

#endif

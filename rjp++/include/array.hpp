/**
	rjp++
	Copyright (C) 2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RJP_ARRAY_HPP
#define RJP_ARRAY_HPP

#include <rjp.h>
#include <cstdlib> //size_t
#include "iterator.hpp"
#include "value.hpp"
#include "integral.hpp"
#include "string_val.hpp"
#include "rjp_util.hpp"
#include "container.hpp"

namespace rjp{

	class object;

	class array_iterator : protected detail::iterator_base
	{
	protected:
		RJP_array_iterator m_it;

	public:
		array_iterator(void) = default;
		array_iterator(RJP_value* v);
		array_iterator(const array_iterator&) = delete;
		array_iterator(array_iterator&& a);
		~array_iterator(void);
		array_iterator& operator=(const array_iterator&) = delete;
		array_iterator& operator=(array_iterator&& o);

		bool operator==(const array_iterator& o)const;
		bool operator!=(const array_iterator& o)const;
		value operator*(void)const;
		value_wrapper operator->(void)const;

		array_iterator& operator++(void);
	};
	class array : public value, private container
	{
	public:
		using iterator = array_iterator;
		using const_iterator = const iterator;
		using size_type = size_t;
	public:
		using value::value;
		array(void);
		array(const value& val);
		array(value&& val);

		array(const array&) = default;
		array(array&&) = default;
		~array(void) = default;
		array& operator=(const array&) = default;
		array& operator=(array&&) = default;

		template<class Val>
		std::decay_t<Val> add(typename std::decay_t<Val>::underlying_type t){
			RJP_value* newelem = create_element(m_value);
			detail::set_to_underlying<std::decay_t<Val>>(newelem, t);
			return std::decay_t<Val>(create_unmanaged(newelem));
		}
		template<class Val = null>
		std::decay_t<Val> add(void){
			RJP_value* newelem = create_element(m_value);
			if constexpr(std::is_same<std::decay_t<Val>,rjp::array>::value)
				set_array_value(newelem);
			else if constexpr(std::is_same<std::decay_t<Val>,rjp::object>::value)
				set_object_value(newelem);
			else if constexpr(std::is_void<typename std::decay_t<Val>::underlying_type>::value)
				set_null_value(newelem);
			else
				detail::set_to_underlying<std::decay_t<Val>>(newelem, 0);
			return std::decay_t<Val>(create_unmanaged(newelem));
		}
		string_val add(const RJP_string&);
		string_val add(RJP_string&&);
		string_val add(const char*, RJP_index len = 0);
		string_val add(rexy::string_view);
		string_val add(string&&);

		value& remove(value& val);
		void destroy(value&& val);

		iterator begin(void);
		iterator end(void);
		const_iterator begin(void)const;
		const_iterator end(void)const;
		const_iterator cbegin(void)const;
		const_iterator cend(void)const;

		size_type size(void)const;

	private:
		static RJP_value* create_element(RJP_value* arr);
	};

}

#endif

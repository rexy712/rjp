/**
	rjp++
	Copyright (C) 2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RJP_STRING_HPP
#define RJP_STRING_HPP

#include <rexy/string.hpp>
#include <rjp.h>

namespace rjp{

	namespace detail{
		struct allocator{
			using value_type = char;
			using size_type = size_t;
			using pointer = char*;
			using const_pointer = const char*;

			char* allocate(size_type n);
			void deallocate(pointer data, size_type n);
		};
	}

	class string : public rexy::basic_string<char,detail::allocator>
	{
	public:
		string(const string&) = default;
		string(string&&) = default;
		string& operator=(const string&) = default;
		string& operator=(string&&) = default;

		using rexy::basic_string<char,detail::allocator>::basic_string;
		string(RJP_value* r);
		using rexy::basic_string<char,detail::allocator>::operator=;
		string& operator=(RJP_value* r);
	};

	string escape(const rexy::string_base<char>& str);
	string escape(const char* c);

}

#endif

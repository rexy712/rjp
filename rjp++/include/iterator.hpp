/**
	rjp++
	Copyright (C) 2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RJP_ITERATOR_BASE_HPP
#define RJP_ITERATOR_BASE_HPP

#include <rjp.h>

#include "member.hpp"
#include "value.hpp"

namespace rjp::detail{
	class iterator_base
	{
	protected:
		class value_wrapper
		{
		private:
			member<void> m_value;
		public:
			value_wrapper(RJP_value* value, bool managed):
				m_value(value, managed){}
			member<void>* operator->(void){
				return &m_value;
			}
		};
	};
}

#endif

/**
	rjp++
	Copyright (C) 2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RJP_UTIL_HPP
#define RJP_UTIL_HPP

#include <rjp.h>
#include "integral.hpp"

namespace rjp{
	template<class To, class From>
	auto convert_to(From&& from){
		return To(std::forward<From>(from));
	}

	template<class To, class From>
	auto steal_as(From&& from){
		return To(std::move(from));
	}

	template<class To, class From>
	auto borrow_as(From&& from){
		return To(const_cast<RJP_value*>(from.raw()), false);
	}

	namespace detail{
		template<class Val>
		void set_to_underlying(RJP_value* val, typename Val::underlying_type);
		template<>
		void set_to_underlying<rjp::integer>(RJP_value* val, RJP_int i);
		template<>
		void set_to_underlying<rjp::dfloat>(RJP_value* val, RJP_float f);
		template<>
		void set_to_underlying<rjp::boolean>(RJP_value* val, RJP_bool b);
	}
}

#endif

/**
	rjp++
	Copyright (C) 2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RJP_STRING_VAL_HPP
#define RJP_STRING_VAL_HPP

#include <rjp.h>
#include <rexy/string.hpp>
#include "string.hpp"
#include "value.hpp"

namespace rjp{

	class string_val : public value
	{
	public:
		using underlying_type = RJP_string;
	public:
		using value::value;
		string_val(rexy::string_view str);
		string_val(string&& str);
		string_val(void);
		string_val(const value& val);
		string_val(value&& val);
		string_val(const value& val, rexy::string_view i);
		string_val(value&& val, rexy::string_view i);
		string_val(const value& val, string&& i);
		string_val(value&& val, string&& i);

		string_val(const string_val&) = default;
		string_val(string_val&&) = default;
		~string_val(void) = default;
		string_val& operator=(const string_val&) = default;
		string_val& operator=(string_val&&) = default;

		rexy::string_view get(void)const;
		string steal(void);
		void set(rexy::string_view str);
		void set(string&& str);

	};

}

#endif

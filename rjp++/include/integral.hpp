/**
	rjp++
	Copyright (C) 2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RJP_INTEGRAL_HPP
#define RJP_INTEGRAL_HPP

#include "value.hpp"
#include <rjp.h>

namespace rjp{

	class integer : public value
	{
	public:
		using underlying_type = RJP_int;
	public:
		using value::value;
		integer(underlying_type i);
		integer(void);
		integer(const value& val);
		integer(value&& val);
		integer(const value& val, underlying_type i);
		integer(value&& val, underlying_type i);

		integer(const integer&) = default;
		integer(integer&&) = default;
		~integer(void) = default;
		integer& operator=(const integer&) = default;
		integer& operator=(integer&&) = default;

		underlying_type get(void)const;
		void set(underlying_type i);

	};
	class dfloat : public value
	{
	public:
		using underlying_type = RJP_float;
	public:
		using value::value;
		dfloat(underlying_type i);
		dfloat(void);
		dfloat(const value& val);
		dfloat(value&& val);
		dfloat(const value& val, underlying_type i);
		dfloat(value&& val, underlying_type i);

		dfloat(const dfloat&) = default;
		dfloat(dfloat&&) = default;
		~dfloat(void) = default;
		dfloat& operator=(const dfloat&) = default;
		dfloat& operator=(dfloat&&) = default;

		underlying_type get(void)const;
		void set(underlying_type i);
	};
	class boolean : public value
	{
	public:
		using underlying_type = RJP_bool;
	public:
		using value::value;
		boolean(underlying_type i);
		boolean(void);
		boolean(const value& val);
		boolean(value&& val);
		boolean(const value& val, underlying_type i);
		boolean(value&& val, underlying_type i);

		boolean(const boolean&) = default;
		boolean(boolean&&) = default;
		~boolean(void) = default;
		boolean& operator=(const boolean&) = default;
		boolean& operator=(boolean&&) = default;

		bool get(void)const;
		void set(bool i);
	};
	class null : public value
	{
	public:
		using underlying_type = void;
	public:
		using value::value;
		null(void);
		null(const value& val);
		null(value&& val);

		null(const null&) = default;
		null(null&&) = default;
		~null(void) = default;
		null& operator=(const null&) = default;
		null& operator=(null&&) = default;

	};

}

#endif

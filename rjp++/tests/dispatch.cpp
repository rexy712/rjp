#include "rjp_internal.hpp"

#include <cstdlib>
#include <cassert>

#define NUM_ELEMENTS 100

rjp::array initialize_array(int num){
	rjp::array root;

	for(int i = 0;i < num;++i){
		switch(rand() % 4){
		case 0:
			root.add<rjp::integer>(rand() % 10);
			break;
		case 1:
			root.add<rjp::boolean>(rand() % 2);
			break;
		case 2:
			root.add<rjp::array>();
			break;
		case 3:
			root.add<rjp::object>();
			break;
		};
	}
	return root;
}

int main(){
	srand(time(0));
	int retval = 0;

	rjp::array root = initialize_array(NUM_ELEMENTS);

	for(auto&& val : root){
		RJP_data_type d = rjp_json_null;
		rjp::dispatch(rjp::dispatcher{
			[&d](const rjp::integer&){d = rjp_json_integer;},
			[&d](const rjp::boolean&){d = rjp_json_boolean;},
			[&d](const rjp::array&){d = rjp_json_array;},
			[&d](const rjp::object&){d = rjp_json_object;},
		}, val);
		if(d != val.type()){
			++retval;
			printf("Dispatch error\n");
		}
	}
	return retval;
}

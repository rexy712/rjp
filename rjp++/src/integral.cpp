/**
	rjp++
	Copyright (C) 2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "integral.hpp"
#include <rjp.h>
#include <utility> //move

namespace rjp{

	integer::integer(underlying_type i):
		value(rjp_new_int(i), true){}
	integer::integer(void):
		integer(0){}
	integer::integer(const value& val):
		value(val)
	{
		if(!m_value)
			return;
		if(rjp_value_type(m_value) != rjp_json_integer)
			rjp_set_int(m_value, rjp_get_int(val.raw()));
	}
	integer::integer(value&& val):
		value(std::move(val))
	{
		if(!m_value)
			return;
		if(rjp_value_type(m_value) != rjp_json_integer)
			rjp_set_int(m_value, rjp_get_int(m_value));
	}
	integer::integer(const value& val, underlying_type i):
		value(val)
	{
		rjp_set_int(m_value, i);
	}
	integer::integer(value&& val, underlying_type i):
		value(std::move(val))
	{
		rjp_set_int(m_value, i);
	}
	integer::underlying_type integer::get(void)const{
		return rjp_get_int(m_value);
	}
	void integer::set(underlying_type i){
		rjp_set_int(m_value, i);
	}

	dfloat::dfloat(underlying_type i):
		value(rjp_new_float(i), true){}
	dfloat::dfloat(void):
		dfloat(0){}
	dfloat::dfloat(const value& val):
		value(val)
	{
		if(!m_value)
			return;
		if(rjp_value_type(m_value) != rjp_json_dfloat)
			rjp_set_float(m_value, rjp_get_float(val.raw()));
	}
	dfloat::dfloat(value&& val):
		value(std::move(val))
	{
		if(!m_value)
			return;
		if(rjp_value_type(m_value) != rjp_json_dfloat)
			rjp_set_float(m_value, rjp_get_float(m_value));
	}
	dfloat::dfloat(const value& val, underlying_type i):
		value(val)
	{
		rjp_set_float(m_value, i);
	}
	dfloat::dfloat(value&& val, underlying_type i):
		value(std::move(val))
	{
		rjp_set_float(m_value, i);
	}

	dfloat::underlying_type dfloat::get(void)const{
		return rjp_get_float(m_value);
	}
	void dfloat::set(underlying_type i){
		rjp_set_float(m_value, i);
	}

	boolean::boolean(underlying_type i):
		value(rjp_new_bool(i), true){}
	boolean::boolean(void):
		boolean(0){}
	boolean::boolean(const value& val):
		value(val)
	{
		if(!m_value)
			return;
		if(rjp_value_type(m_value) != rjp_json_boolean)
			rjp_set_bool(m_value, rjp_get_bool(val.raw()));
	}
	boolean::boolean(value&& val):
		value(std::move(val))
	{
		if(!m_value)
			return;
		if(rjp_value_type(m_value) != rjp_json_boolean)
			rjp_set_bool(m_value, rjp_get_bool(m_value));
	}
	boolean::boolean(const value& val, underlying_type i):
		value(val)
	{
		rjp_set_bool(m_value, i);
	}
	boolean::boolean(value&& val, underlying_type i):
		value(std::move(val))
	{
		rjp_set_bool(m_value, i);
	}

	bool boolean::get(void)const{
		return rjp_get_bool(m_value);
	}
	void boolean::set(bool i){
		rjp_set_bool(m_value, i);
	}

	null::null(void):
		value(rjp_new_null(), true){}
	null::null(const value& val):
		value(val)
	{
		if(!m_value)
			return;
		rjp_set_null(m_value);
	}
	null::null(value&& val):
		value(std::move(val))
	{
		rjp_set_null(m_value);
	}

}


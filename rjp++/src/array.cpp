/**
	rjp++
	Copyright (C) 2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "array.hpp"
#include <rjp.h>
#include <utility> //move, swap

namespace rjp{

	array::array(void):
		value(rjp_new_array(), true){}
	array::array(const value& val):
		value(val)
	{
		if(!m_value)
			return;
		if(rjp_value_type(m_value) != rjp_json_array)
			rjp_set_array(m_value);
	}
	array::array(value&& val):
		value(std::move(val))
	{
		if(!m_value)
			return;
		if(rjp_value_type(m_value) != rjp_json_array)
			rjp_set_array(m_value);
	}
	string_val array::add(const RJP_string& s){
		RJP_value* newelem = rjp_new_element(m_value);
		rjp_set_string(newelem, s.value, s.length);
		return string_val(create_unmanaged(newelem));
	}
	string_val array::add(RJP_string&& s){
		RJP_value* newelem = rjp_new_element(m_value);
		rjp_set_string_steal(newelem, s.value, s.length);
		return string_val(create_unmanaged(newelem));
	}
	string_val array::add(const char* c, RJP_index len){
		RJP_value* newelem = rjp_new_element(m_value);
		rjp_set_string(newelem, c, len);
		return string_val(create_unmanaged(newelem));
	}
	string_val array::add(rexy::string_view s){
		return add(s.data(), s.length());
	}
	string_val array::add(string&& s){
		RJP_value* newelem = rjp_new_element(m_value);
		auto len = s.length();
		rjp_set_string_steal(newelem, s.release(), len);
		return string_val(create_unmanaged(newelem));
	}

	value& array::remove(value& val){
		rjp_remove_element(m_value, val.raw());
		add_management(val);
		return val;
	}
	void array::destroy(value&& val){
		rjp_free_element(m_value, val.raw());
	}
	array::iterator array::begin(void){
		return iterator(m_value);
	}
	array::iterator array::end(void){
		return iterator();
	}
	array::const_iterator array::begin(void)const{
		return const_iterator(m_value);
	}
	array::const_iterator array::end(void)const{
		return iterator();
	}
	array::const_iterator array::cbegin(void)const{
		return const_iterator(m_value);
	}
	array::const_iterator array::cend(void)const{
		return iterator();
	}
	array::size_type array::size(void)const{
		return rjp_num_elements(m_value);
	}
	RJP_value* array::create_element(RJP_value* arr){
		return rjp_new_element(arr);
	}


	array_iterator::array_iterator(RJP_value* v){
		rjp_init_array_iterator(&m_it, v);
	}
	array_iterator::array_iterator(array_iterator&& a):
		m_it(a.m_it)
	{
		a.m_it = RJP_array_iterator{};
	}
	array_iterator::~array_iterator(void){
		rjp_delete_array_iterator(&m_it);
	}
	array_iterator& array_iterator::operator=(array_iterator&& o){
		std::swap(m_it, o.m_it);
		return *this;
	}

	bool array_iterator::operator==(const array_iterator& o)const{
		return rjp_array_iterator_current(&m_it) == rjp_array_iterator_current(&o.m_it);
	}
	bool array_iterator::operator!=(const array_iterator& o)const{
		return !(*this == o);
	}
	value array_iterator::operator*(void)const{
		return value(rjp_array_iterator_current(&m_it), false);
	}
	auto array_iterator::operator->(void)const -> value_wrapper{
		return value_wrapper(rjp_array_iterator_current(&m_it), false);
	}

	array_iterator& array_iterator::operator++(void){
		rjp_array_iterator_next(&m_it);
		return *this;
	}


}

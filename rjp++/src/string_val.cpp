/**
	rjp++
	Copyright (C) 2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "string_val.hpp"
#include <rjp.h>
#include <utility> //move

namespace rjp{

	using namespace rexy::str_literals;

	string_val::string_val(rexy::string_view str):
		value(rjp_new_string(str.data(), str.length()), true){}
	string_val::string_val(string&& str):
		value(rjp_new_string_steal(str.data(), str.length()), true)
	{
		str.release();
	}
	string_val::string_val(void):
		string_val(""_sv){}
	string_val::string_val(const value& val):
		value(val)
	{
		if(!m_value)
			return;
		if(rjp_value_type(m_value) != rjp_json_string)
			rjp_set_string(m_value, "", 0);
	}
	string_val::string_val(value&& val):
		value(std::move(val))
	{
		if(!m_value)
			return;
		if(rjp_value_type(m_value) != rjp_json_string)
			rjp_set_string(m_value, "", 0);
	}
	string_val::string_val(const value& val, rexy::string_view i):
		value(val)
	{
		rjp_set_string(m_value, i.data(), i.length());
	}
	string_val::string_val(value&& val, rexy::string_view i):
		value(std::move(val))
	{
		rjp_set_string(m_value, i.data(), i.length());
	}
	string_val::string_val(const value& val, string&& i):
		value(val)
	{
		auto length = i.length();
		rjp_set_string_steal(m_value, i.release(), length);
	}
	string_val::string_val(value&& val, string&& i):
		value(std::move(val))
	{
		auto length = i.length();
		rjp_set_string_steal(m_value, i.release(), length);
	}

	rexy::string_view string_val::get(void)const{
		const RJP_string* str = rjp_get_cstring(m_value);
		return rexy::string_view(str->value, str->length);
	}
	string string_val::steal(void){
		return string(m_value);
	}
	void string_val::set(rexy::string_view str){
		rjp_set_string(m_value, str.data(), str.length());
	}
	void string_val::set(string&& str){
		auto length = str.length();
		rjp_set_string_steal(m_value, str.release(), length);
	}

}

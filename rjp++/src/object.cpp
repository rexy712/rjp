/**
	rjp++
	Copyright (C) 2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "object.hpp"
#include <rjp.h>
#include <utility> //move

namespace rjp{
	object::object(void):
		value(rjp_new_object(), true){}
	object::object(const value& val):
		value(val)
	{
		if(!m_value)
			return;
		if(rjp_value_type(m_value) != rjp_json_object)
			rjp_set_object(m_value);
	}
	object::object(value&& val):
		value(std::move(val))
	{
		if(!m_value)
			return;
		if(rjp_value_type(m_value) != rjp_json_object)
			rjp_set_object(m_value);
	}
	member<string_val> object::add(rexy::string_view key, const RJP_string& s){
		RJP_value* newmem = rjp_new_member(m_value, key.data(), key.length());
		rjp_set_string(newmem, s.value, s.length);
		return create_unmanaged(newmem);
	}
	member<string_val> object::add(rexy::string_view key, RJP_string&& s){
		RJP_value* newmem = rjp_new_member(m_value, key.data(), key.length());
		rjp_set_string_steal(newmem, s.value, s.length);
		return create_unmanaged(newmem);
	}
	member<string_val> object::add(rexy::string_view key, const char* s, RJP_index len){
		RJP_value* newmem = rjp_new_member(m_value, key.data(), key.length());
		rjp_set_string(newmem, s, len);
		return create_unmanaged(newmem);
	}
	member<string_val> object::add(rexy::string_view key, rexy::string_view s){
		RJP_value* newmem = rjp_new_member(m_value, key.data(), key.length());
		rjp_set_string(newmem, s.data(), s.length());
		return create_unmanaged(newmem);
	}
	member<string_val> object::add(rexy::string_view key, string&& s){
		RJP_value* newmem = rjp_new_member(m_value, key.data(), key.length());
		auto len = s.length();
		rjp_set_string_steal(newmem, s.release(), len);
		return create_unmanaged(newmem);
	}

	member<string_val> object::add(string&& key, const RJP_string& s){
		RJP_value* newmem = rjp_new_member_steal_key(m_value, key.data(), key.length());
		rjp_set_string(newmem, s.value, s.length);
		return create_unmanaged(newmem);
	}
	member<string_val> object::add(string&& key, RJP_string&& s){
		RJP_value* newmem = rjp_new_member_steal_key(m_value, key.data(), key.length());
		rjp_set_string_steal(newmem, s.value, s.length);
		return create_unmanaged(newmem);
	}
	member<string_val> object::add(string&& key, const char* s, RJP_index len){
		RJP_value* newmem = rjp_new_member_steal_key(m_value, key.data(), key.length());
		rjp_set_string(newmem, s, len);
		return create_unmanaged(newmem);
	}
	member<string_val> object::add(string&& key, rexy::string_view s){
		RJP_value* newmem = rjp_new_member_steal_key(m_value, key.data(), key.length());
		rjp_set_string(newmem, s.data(), s.length());
		return create_unmanaged(newmem);
	}
	member<string_val> object::add(string&& key, string&& s){
		RJP_value* newmem = rjp_new_member_steal_key(m_value, key.data(), key.length());
		auto len = s.length();
		rjp_set_string_steal(newmem, s.release(), len);
		return create_unmanaged(newmem);
	}

	value object::remove(rexy::string_view key){
		RJP_value* removed = rjp_remove_member_by_key(m_value, key.data());
		return create_managed(removed);
	}
	value& object::remove(value& val){
		rjp_remove_member(m_value, val.raw());
		add_management(val);
		return val;
	}

	void object::destroy(rexy::string_view key){
		rjp_free_member_by_key(m_value, key.data());
	}
	void object::destroy(value&& val){
		rjp_free_member(m_value, val.raw());
	}

	bool object::has_child(const value& val){
		return val.parent().raw() == m_value;
	}
	object::iterator object::begin(void){
		return iterator(m_value);
	}
	object::const_iterator object::begin(void)const{
		return const_iterator(m_value);
	}
	object::iterator object::end(void){
		return iterator();
	}
	object::const_iterator object::end(void)const{
		return const_iterator();
	}
	object::const_iterator object::cbegin(void)const{
		return const_iterator(m_value);
	}
	object::const_iterator object::cend(void)const{
		return const_iterator();
	}
	object::size_type object::size(void)const{
		return rjp_num_members(m_value);
	}
	value object::search(rexy::string_view key)const{
		RJP_value* result = rjp_search_member(m_value, key.data());
		return create_unmanaged(result);
	}

	RJP_value* object::add_member_impl(rexy::string_view key){
		return rjp_new_member(m_value, key.data(), key.length());
	}
	RJP_value* object::add_member_impl(string&& key){
		auto length = key.length();
		return rjp_new_member_steal_key(m_value, key.release(), length);
	}


	object_iterator::object_iterator(void):
		m_it(){}
	object_iterator::object_iterator(RJP_value* v){
		rjp_init_object_iterator(&m_it, v);
	}
	object_iterator::object_iterator(object_iterator&& o):
		m_it(o.m_it)
	{
		o.m_it = RJP_object_iterator{};
	}
	object_iterator::~object_iterator(void){
		rjp_delete_object_iterator(&m_it);
	}
	object_iterator& object_iterator::operator=(object_iterator&& o){
		std::swap(m_it, o.m_it);
		return *this;
	}

	bool object_iterator::operator==(const object_iterator& o)const{
		return rjp_object_iterator_current(&m_it) == rjp_object_iterator_current(&o.m_it);
	}
	bool object_iterator::operator!=(const object_iterator& o)const{
		return !(*this == o);
	}
	member<void> object_iterator::operator*(void)const{
		return member<void>(rjp_object_iterator_current(&m_it), false);
	}
	auto object_iterator::operator->(void)const -> value_wrapper{
		return value_wrapper(rjp_object_iterator_current(&m_it), false);
	}

	object_iterator& object_iterator::operator++(void){
		rjp_object_iterator_next(&m_it);
		return *this;
	}

}

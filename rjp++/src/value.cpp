/**
	rjp++
	Copyright (C) 2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "value.hpp"
#include <rjp.h>
#include <utility> //swap, move, exchange

namespace rjp{

	value::value(RJP_value* val, bool managed):
		m_value(val),
		m_managed(managed){}
	value::value(void):
		m_value(rjp_new_null()){}
	value::value(const value& val):
		value()
	{
		rjp_copy_value(m_value, val.m_value);
	}
	value::value(value&& val):
		m_value(std::exchange(val.m_value, nullptr)),
		m_managed(val.m_managed){}
	value::~value(void){
		if(m_managed)
			rjp_free_value(m_value);
	}

	value& value::operator=(const value& val){
		value tmp(val);
		return (*this = std::move(tmp));
	}
	value& value::operator=(value&& val){
		std::swap(val.m_value, m_value);
		std::swap(val.m_managed, m_managed);
		return *this;
	}
	value::operator bool(void)const{
		return m_value != nullptr;
	}
	bool value::valid(void)const{
		return m_value != nullptr;
	}

	const RJP_value* value::raw(void)const{
		return m_value;
	}
	RJP_value* value::raw(void){
		return m_value;
	}

	RJP_data_type value::type(void)const{
		return rjp_value_type(m_value);
	}
	detail::vget_proxy value::get(void)const{
		return detail::vget_proxy(this);
	}


	value value::parent(void)const{
		return create_unmanaged(rjp_value_parent(m_value));
	}
	bool value::is_child_of(const value& val)const{
		return rjp_value_parent(m_value) == val.raw();
	}

	string value::to_json(int flags){
		string s;
		s.reset(rjp_to_json(m_value, flags));
		return s;
	}

	value value::create_unmanaged(RJP_value* val){
		return value(val, false);
	}
	void value::remove_management(value& val){
		val.m_managed = false;
	}
	value value::create_managed(RJP_value* val){
		return value(val, true);
	}
	void value::add_management(value& val){
		val.m_managed = true;
	}
}


/**
	rjp++
	Copyright (C) 2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "value.hpp"
#include "integral.hpp"
#include "string_val.hpp"
#include "string.hpp"
#include "rjp_util.hpp"

namespace rjp::detail{

	vget_proxy::vget_proxy(const rjp::value* v):
		m_value(v){}
	vget_proxy::operator int(void)const{
		return rjp::borrow_as<rjp::integer>(*m_value).get();
	}
	vget_proxy::operator bool(void)const{
		return rjp::borrow_as<rjp::boolean>(*m_value).get();
	}
	vget_proxy::operator rexy::string_view(void)const{
		return rjp::borrow_as<rjp::string_val>(*m_value).get();
	}
	vget_proxy::operator double(void)const{
		return rjp::borrow_as<rjp::dfloat>(*m_value).get();
	}

}

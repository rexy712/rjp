/**
	rjp++
	Copyright (C) 2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "container.hpp"
#include <rjp.h>

namespace rjp{

	void container::set_array_value(RJP_value* v){
		rjp_set_array(v);
	}
	void container::set_object_value(RJP_value* v){
		rjp_set_object(v);
	}
	void container::set_null_value(RJP_value* v){
		rjp_set_null(v);
	}

}

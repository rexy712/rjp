/**
	rjp++
	Copyright (C) 2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <rexy/string.hpp>
#include <rjp.h>
#include "string.hpp"
#include <utility> //exchange

namespace rjp{

	string::string(RJP_value* r):
		rexy::basic_string<char,detail::allocator>(rexy::steal(r ? rjp_get_string(r)->value : nullptr),
	                                             r ? rjp_get_string(r)->length : 0,
	                                             r ? rjp_get_string(r)->length : 0)
	{
		if(r){
			RJP_string* str = rjp_get_string(r);
			str->value = nullptr;
			str->length = 0;
			rjp_set_null(r);
		}
	}
	string& string::operator=(RJP_value* r){
		if(!r)
			return *this;
		reset();
		RJP_string* str = rjp_get_string(r);
		set_long_ptr(std::exchange(str->value, nullptr));
		set_long_length(str->length);
		set_long_capacity(str->length);
		str->length = 0;
		return *this;
	}

	string escape(const rexy::string_base<char>& str){
		return escape(str.c_str());
	}
	string escape(const char* c){
		RJP_string tmp = rjp_escape(c);
		return string(rexy::steal<char*>(tmp.value), tmp.length);
	}
}

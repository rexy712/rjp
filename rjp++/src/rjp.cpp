/**
	rjp++
	Copyright (C) 2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <rjp.h>
#include <rexy/string_base.hpp>

#include "rjp_internal.hpp"
#include "parse.hpp"
#include "string.hpp"
#include "value.hpp"
#include "rjp_util.hpp"

namespace rjp{

	parse_res::parse_res(RJP_value* val, const RJP_parse_error& err):
		m_value(val), m_err(err){}
	parse_res::parse_res(parse_res&& p):
		m_value(std::exchange(p.m_value, nullptr)), m_err(std::move(p.m_err)){}

	parse_res::~parse_res(void){
		rjp_free_value(m_value);
		if(m_err.parsestate)
			rjp_delete_parse_error(&m_err);
	}

	parse_res::operator bool(void)const{
		return valid();
	}
	bool parse_res::valid(void)const{
		return m_value != nullptr;
	}

	value parse_res::get_value(void){
		return value(std::exchange(m_value, nullptr), true);
	}
	string parse_res::errstr(void)const{
		return string(rexy::steal(rjp_parse_error_to_string(&m_err)));
	}

	string to_json(const value& val, int format){
		string s;
		s.reset(rjp_to_json(val.raw(), format));
		return s;
	}
	parse_res parse_json(rexy::string_view str, RJP_parse_flag flags){
		return parse_json(str.data(), flags);
	}
	parse_res parse_json(const char* str, RJP_parse_flag flags){
		RJP_parse_error err = {};
		RJP_value* v = rjp_parse(str, flags, &err);
		return parse_res(v, err);
	}
	namespace detail{
		int irjp_parse_callback(char* dest, int size, void* userdata){
			invoker* inv = static_cast<invoker*>(userdata);
			return inv->run(dest, size);
		}
		RJP_value* parse_cback(RJP_parse_flag f, RJP_parse_callback* cb, RJP_parse_error& err){
			return rjp_parse_cback(f, cb, &err);
		}
		RJP_value* parse_cback(RJP_parse_flag f, RJP_parse_callback* cb){
			return rjp_parse_cback(f, cb, NULL);
		}

		template<>
		void set_to_underlying<rjp::integer>(RJP_value* val, RJP_int i){
			rjp_set_int(val, i);
		}
		template<>
		void set_to_underlying<rjp::dfloat>(RJP_value* val, RJP_float f){
			rjp_set_float(val, f);
		}
		template<>
		void set_to_underlying<rjp::boolean>(RJP_value* val, RJP_bool b){
			rjp_set_bool(val, b);
		}
	}
}

/**
	rjp++
	Copyright (C) 2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <rjp.h>
#include "string.hpp"
#include <cstring> //memcpy

namespace rjp::detail{
	void allocator::deallocate(pointer data, size_type){
		rjp_free(data);
	}
	auto allocator::allocate(size_type size) -> pointer{
		return reinterpret_cast<char*>(rjp_alloc(size));
	}
}

#include <rjp.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv){
	if(argc != 2){
		fprintf(stderr, "Requires exactly 1 argument\n");
		return 1;
	}

	FILE* fp = fopen(argv[1], "rb");
	if(!fp){
		fprintf(stderr, "Unable to open file\n");
		return 1;
	}
	fseek(fp, 0, SEEK_END);
	int buflen = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	char* buffer = malloc(buflen+1);
	fread(buffer, 1, buflen, fp);
	buffer[buflen] = 0;
	fclose(fp);

	//Read in json argument allowing all RJP extensions (comments, trailing comma)
	RJP_value* root = rjp_parse(buffer, RJP_PARSE_ALL_EXT, NULL);

	//returns NULL on error
	if(!root){
		fprintf(stderr, "Invalid JSON\n");
		return 1;
	}
	printf("Valid JSON\n");

	//clean memory
	free(buffer);
	rjp_free_value(root);
}

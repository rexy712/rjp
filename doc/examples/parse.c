#include <rjp.h>
#include <stdio.h>

int main(int argc, char** argv){
	if(argc != 2){
		fprintf(stderr, "Requires exactly 1 argument\n");
		return 1;
	}

	//Read in json argument allowing all RJP extensions (comments, trailing comma)
	RJP_value* root = rjp_parse(argv[1], RJP_PARSE_ALL_EXT, NULL);

	//returns NULL on error
	if(!root){
		fprintf(stderr, "Invalid JSON\n");
		return 1;
	}
	printf("Valid JSON\n");

	//check value's type
	if(rjp_value_type(root) == rjp_json_object){

		//Initialize an object iterator for the root value
		RJP_object_iterator it;
		rjp_init_object_iterator(&it, root);

		//iterate over all members of root, printing their keys
		for(RJP_value* curr = rjp_object_iterator_current(&it);curr;curr = rjp_object_iterator_next(&it)){
			printf("Have member with key \"%s\"\n", rjp_member_key(curr)->value);
		}
		rjp_delete_object_iterator(&it);
	}

	//clean memory
	rjp_free_value(root);
}

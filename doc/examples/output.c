#include <rjp.h>
#include <stdio.h>

int main(){
	//create an initial value
	RJP_value* root = rjp_new_object();

	//add a member and set its type
	RJP_value* member = rjp_new_member(root, "key", 0);
	rjp_set_int(member, 5);

	//output into rjp_alloc'd buffer
	char* output = rjp_to_json(root, RJP_FORMAT_NONE);
	printf("%s\n", output);

	//free memory
	rjp_free(output);
	rjp_free_value(root);
}

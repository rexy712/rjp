/**
	rjp
	Copyright (C) 2018-2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RJP_LEX_H
#define RJP_LEX_H

#include "rjp.h"

#define RJP_LEX_CBACK_BUFFER_SIZE 512
#define RJP_LEX_CBACK_STR_SIZE    64

#define RJP_LEX_TYPE_NORMAL 0
#define RJP_LEX_TYPE_CBACK  1

#define rjp_lex_accept 1
//DFA states. odd numbers are accepting states
typedef enum RJP_lex_category{
	rjp_lex_start = 0,
	rjp_lex_obracket = 3,
	rjp_lex_obrace   = 5,
	rjp_lex_cbracket = 7,
	rjp_lex_cbrace   = 9,
	rjp_lex_spaces   = 11,
	rjp_lex_quote    = 12,
	rjp_lex_t        = 14,
	rjp_lex_tr       = 16,
	rjp_lex_tru      = 18,
	rjp_lex_true     = 19,
	rjp_lex_f        = 20,
	rjp_lex_fa       = 22,
	rjp_lex_fal      = 24,
	rjp_lex_fals     = 26,
	rjp_lex_false    = 27,
	rjp_lex_n        = 28,
	rjp_lex_nu       = 30,
	rjp_lex_nul      = 32,
	rjp_lex_null     = 33,
	rjp_lex_escaped  = 34,
	rjp_lex_string   = 35,
	rjp_lex_comma    = 37,
	rjp_lex_colon    = 39,
	rjp_lex_number   = 41,
	rjp_lex_decimal  = 42,
	rjp_lex_fnumber  = 43,
	rjp_lex_fnum_e   = 44,
	rjp_lex_sci_num  = 45,
	rjp_lex_slash    = 46,
	rjp_lex_line_comment  = 47,
	rjp_lex_signed_number   = 49,
	rjp_lex_sci_num_signed  = 51,
	rjp_lex_newlines = 53,
	rjp_lex_block_comment_start = 54,
	rjp_lex_block_comment_end1 = 56,
	rjp_lex_block_comment = 57,
	rjp_lex_invalid  = 1000,
	rjp_lex_unrecognized_word = 1002,
	rjp_lex_end = 1004,
}RJP_lex_category;

typedef struct RJP_lex_state{
	char* str;             //must hold value parser will use to create tokens. eg contents of strings
	char* buff;            //holds temporary data in callback based lexer
	RJP_index strcap;      //capacity of str. used in callback lexer
	RJP_index buffl;       //length of buff currently in use. used in callback lexer
	RJP_index buffcap;     //capacity of buff. used in callback lexer
	RJP_index buffpos;     //current position in buff being read. used in callback lexer
	RJP_lex_category node; //tracks current dfa state
	RJP_index length;      //length of current token which parser will utilize
	RJP_index offset;      //offset in the str buffer that the parser should start from. must be 0 in callback lexer
	int type;
}RJP_lex_state;

void irjp_init_lex_cback_state(RJP_lex_state* state);
void irjp_init_lex_state(RJP_lex_state* state);
void irjp_delete_lex_state(RJP_lex_state* state);
RJP_lex_category irjp_lex(RJP_lex_state* state);
RJP_lex_category irjp_lex_cback(RJP_lex_state* state, RJP_parse_callback* cbacks);

#endif

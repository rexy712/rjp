/**
	rjp
	Copyright (C) 2018-2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RJP_H_INCLUDED
#define RJP_H_INCLUDED

#ifdef __cplusplus
extern "C"{
#endif

#ifdef __cplusplus
#	if __cplusplus >= 201402L
#		define DEPRECATED(str) [[deprecated(str)]]
#	elif defined(__GNUC__) || defined(__clang__)
#		define DEPRECATED(str) __attribute__((deprecated(str)))
# elif __cplusplus >= 201103L
#		define DEPRECATED(str) [[deprecated]]
#	else
#		define DEPRECATED(str)
#	endif
#elif __STDC_VERSION__ > 201710L
#	define DEPRECATED(str) [[deprecated(str)]]
#elif defined(__GNUC__) || defined(__clang__)
#	define DEPRECATED(str) __attribute__((deprecated(str)))
#else
#	define DEPRECATED(str)
#endif

#ifndef RJP_int
#	include <stdint.h>
#	define RJP_int int_fast64_t
#endif
#ifndef RJP_index
#	include <stddef.h>
#	define RJP_index size_t
#endif
#ifndef RJP_float
#	define RJP_float double
#endif
#ifndef RJP_bool
#	define RJP_bool char
#endif

typedef enum RJP_format_flag{
	RJP_FORMAT_NONE = 0,
	RJP_FORMAT_KEY_SPACES = 1,
	RJP_FORMAT_NEWLINES = 2,
	RJP_FORMAT_TABBED_LINES = RJP_FORMAT_NEWLINES | 4,
	RJP_FORMAT_COMMA_SPACES = 8,
	RJP_FORMAT_PRETTY = RJP_FORMAT_KEY_SPACES | RJP_FORMAT_NEWLINES | RJP_FORMAT_TABBED_LINES
}RJP_format_flag;

typedef enum RJP_parse_flag{
	RJP_PARSE_NO_EXT = 0,
	RJP_PARSE_ALLOW_COMMENTS = 1,
	RJP_PARSE_ALLOW_TRAILING_COMMA = 2,
	RJP_PARSE_ALL_EXT = RJP_PARSE_ALLOW_COMMENTS | RJP_PARSE_ALLOW_TRAILING_COMMA
}RJP_parse_flag;

typedef enum RJP_data_type{
	rjp_json_object  = 1,
	rjp_json_ordered_object = 3,
	rjp_json_string  = 2,
	rjp_json_integer = 4,
	rjp_json_dfloat  = 8,
	rjp_json_boolean = 16,
	rjp_json_array   = 32,
	rjp_json_null    = 64
}RJP_data_type;


typedef struct RJP_value RJP_value;

typedef struct RJP_string{
	char* value;
	RJP_index length;
}RJP_string;

struct RJP_object_iterator_impl;
typedef struct RJP_object_iterator{
	union{
		struct RJP_object_iterator_impl* it;
		RJP_value* current;
	};
	RJP_data_type type;
}RJP_object_iterator;

typedef struct RJP_array_iterator{
	RJP_value* current;
}RJP_array_iterator;

typedef struct RJP_parse_callback{
	int(*read)(char*,int,void*);
	void* data;
}RJP_parse_callback;
typedef struct RJP_parse_error{
	void* parsestate;
	int errcode;
	int row, column;
}RJP_parse_error;
typedef struct RJP_memory_fns{
	void (*free)(void*);
	void* (*alloc)(RJP_index);
}RJP_memory_fns;

/***************** NON OBJECT OPERATIONS *******************/
void* rjp_alloc(RJP_index nbytes);
void* rjp_calloc(RJP_index num, RJP_index nbytes);
void rjp_free(void* dest);

RJP_index rjp_escape_strcpy(char* dest, const char* src);
RJP_index rjp_escape_strlen(const char* str);
RJP_string rjp_escape(const char* src);

/***************** GENERIC OPERATIONS *******************/
RJP_value* rjp_simple_parse(const char* str);
RJP_value* rjp_parse(const char* str, int flags, RJP_parse_error* err);
RJP_value* rjp_parse_cback(int flags, RJP_parse_callback* cbacks, RJP_parse_error* err);
RJP_value* rjp_parse_c(const char* str, int flags, RJP_parse_error* err, const RJP_memory_fns* fns);
RJP_value* rjp_parse_cback_c(int flags, RJP_parse_callback* cbacks, RJP_parse_error* err, const RJP_memory_fns* fns);
char* rjp_to_json(const RJP_value* root, int pretty);
char* rjp_to_json_c(const RJP_value* root, int pretty, const RJP_memory_fns* fns);

char* rjp_parse_error_to_string(const RJP_parse_error* err);
char* rjp_parse_error_to_string_c(const RJP_parse_error* err, const RJP_memory_fns* fns);
void rjp_delete_parse_error(RJP_parse_error* err);
void rjp_delete_parse_error_c(RJP_parse_error* err, const RJP_memory_fns* fns);

RJP_value* rjp_new_null(void);
RJP_value* rjp_new_int(RJP_int val);
RJP_value* rjp_new_float(RJP_float val);
RJP_value* rjp_new_bool(RJP_bool val);
RJP_value* rjp_new_string(const char* val, RJP_index length);
RJP_value* rjp_new_string_steal(char* val, RJP_index length);
RJP_value* rjp_new_object(void);
RJP_value* rjp_new_ordered_object(void);
RJP_value* rjp_new_array(void);

RJP_value* rjp_new_null_c(const RJP_memory_fns* fns);
RJP_value* rjp_new_int_c(RJP_int val, const RJP_memory_fns* fns);
RJP_value* rjp_new_float_c(RJP_float val, const RJP_memory_fns* fns);
RJP_value* rjp_new_bool_c(RJP_bool val, const RJP_memory_fns* fns);
RJP_value* rjp_new_string_c(const char* val, RJP_index length, const RJP_memory_fns* fns);
RJP_value* rjp_new_string_steal_c(char* val, RJP_index length, const RJP_memory_fns* fns);
RJP_value* rjp_new_object_c(const RJP_memory_fns* fns);
RJP_value* rjp_new_ordered_object_c(const RJP_memory_fns* fns);
RJP_value* rjp_new_array_c(const RJP_memory_fns* fns);

void rjp_free_value(RJP_value* root);
void rjp_free_value_c(RJP_value* root, const RJP_memory_fns* fns);

RJP_value* rjp_copy_value(RJP_value* dest, const RJP_value* src);
RJP_value* rjp_copy_value_c(RJP_value* dest, const RJP_value* src, const RJP_memory_fns* fns);
RJP_value* rjp_move_value(RJP_value* dest, RJP_value* src);

RJP_value* rjp_set_null(RJP_value* v);
RJP_value* rjp_set_int(RJP_value* v, RJP_int val);
RJP_value* rjp_set_float(RJP_value* v, RJP_float val);
RJP_value* rjp_set_bool(RJP_value* v, RJP_bool val);
RJP_value* rjp_set_string(RJP_value* v, const char* val, RJP_index length);
RJP_value* rjp_set_string_steal(RJP_value* v, char* val, RJP_index length);
RJP_value* rjp_set_object(RJP_value* v);
RJP_value* rjp_set_ordered_object(RJP_value* v);
RJP_value* rjp_set_array(RJP_value* v);

RJP_value* rjp_set_null_c(RJP_value* v, const RJP_memory_fns* fns);
RJP_value* rjp_set_int_c(RJP_value* v, RJP_int val, const RJP_memory_fns* fns);
RJP_value* rjp_set_float_c(RJP_value* v, RJP_float val, const RJP_memory_fns* fns);
RJP_value* rjp_set_bool_c(RJP_value* v, RJP_bool val, const RJP_memory_fns* fns);
RJP_value* rjp_set_string_c(RJP_value* v, const char* val, RJP_index length, const RJP_memory_fns* fns);
RJP_value* rjp_set_string_steal_c(RJP_value* v, char* val, RJP_index length, const RJP_memory_fns* fns);
RJP_value* rjp_set_object_c(RJP_value* v, const RJP_memory_fns* fns);
RJP_value* rjp_set_ordered_object_c(RJP_value* v, const RJP_memory_fns* fns);
RJP_value* rjp_set_array_c(RJP_value* v, const RJP_memory_fns* fns);

RJP_float rjp_get_float(const RJP_value* value);
RJP_int rjp_get_int(const RJP_value* value);
RJP_bool rjp_get_bool(const RJP_value* value);
RJP_string* rjp_get_string(RJP_value* value);
const RJP_string* rjp_get_cstring(const RJP_value* value);


/***************** OBJECT/ARRAY SHARED OPERATIONS *******************/
RJP_value* rjp_value_parent(const RJP_value* element);
RJP_data_type rjp_value_type(const RJP_value* value);

/***************** OBJECT OPERATIONS *******************/
RJP_value* rjp_new_member(RJP_value* dest, const char* key, RJP_index keylen);
RJP_value* rjp_new_member_steal_key(RJP_value* dest, char* key, RJP_index keylen);
RJP_value* rjp_add_member(RJP_value* dest, const char* key, RJP_index keylen, RJP_value* src);
RJP_value* rjp_add_member_steal_key(RJP_value* dest, char* key, RJP_index keylen, RJP_value* src);

RJP_value* rjp_new_member_c(RJP_value* dest, const char* key, RJP_index keylen, const RJP_memory_fns* fns);
RJP_value* rjp_new_member_steal_key_c(RJP_value* dest, char* key, RJP_index keylen, const RJP_memory_fns* fns);
RJP_value* rjp_add_member_c(RJP_value* dest, const char* key, RJP_index keylen, RJP_value* src, const RJP_memory_fns* fns);
RJP_value* rjp_add_member_steal_key_c(RJP_value* dest, char* key, RJP_index keylen, RJP_value* src, const RJP_memory_fns* fns);

RJP_value* rjp_remove_member_by_key(RJP_value* obj, const char* key);
RJP_value* rjp_remove_member(RJP_value* obj, RJP_value* member);
void rjp_free_member_by_key(RJP_value* obj, const char* key);
void rjp_free_member(RJP_value* obj, RJP_value* member);
RJP_value* rjp_remove_member_by_key_c(RJP_value* obj, const char* key, const RJP_memory_fns* fns);
RJP_value* rjp_remove_member_c(RJP_value* obj, RJP_value* member, const RJP_memory_fns* fns);
void rjp_free_member_by_key_c(RJP_value* obj, const char* key, const RJP_memory_fns* fns);
void rjp_free_member_c(RJP_value* obj, RJP_value* member, const RJP_memory_fns* fns);

RJP_value* rjp_set_key_steal(RJP_value* dest, char* key, RJP_index keylen);
RJP_value* rjp_set_key(RJP_value* dest, const char* key, RJP_index keylen);
RJP_value* rjp_set_key_steal_c(RJP_value* dest, char* key, RJP_index keylen, const RJP_memory_fns* fns);
RJP_value* rjp_set_key_c(RJP_value* dest, const char* key, RJP_index keylen, const RJP_memory_fns* fns);

RJP_index rjp_num_members(const RJP_value* object);
const RJP_string* rjp_member_key(const RJP_value* member);
RJP_value* rjp_search_member(const RJP_value* object, const char* search);

RJP_value* rjp_object_to_ordered(RJP_value* object);
RJP_value* rjp_object_to_unordered(RJP_value* object);
RJP_value* rjp_object_to_ordered_c(RJP_value* object, const RJP_memory_fns* fns);
RJP_value* rjp_object_to_unordered_c(RJP_value* object, const RJP_memory_fns* fns);

/***************** OBJECT ITERATOR OPERATIONS *******************/
void rjp_init_object_iterator(RJP_object_iterator* iter, const RJP_value* object);
void rjp_delete_object_iterator(RJP_object_iterator* it);
RJP_value* rjp_object_iterator_current(const RJP_object_iterator* it);
RJP_value* rjp_object_iterator_next(RJP_object_iterator* it);
RJP_value* rjp_object_iterator_peek(const RJP_object_iterator* it);

/***************** ARRAY OPERATIONS *******************/
RJP_value* rjp_new_element(RJP_value* dest);
RJP_value* rjp_add_element(RJP_value* dest, RJP_value* src);
RJP_value* rjp_new_element_c(RJP_value* dest, const RJP_memory_fns* fns);
RJP_value* rjp_add_element_c(RJP_value* dest, RJP_value* src, const RJP_memory_fns* fns);
RJP_value* rjp_remove_element(RJP_value* arr, RJP_value* elem);
void rjp_free_element(RJP_value* arr, RJP_value* elem);
void rjp_free_element_c(RJP_value* arr, RJP_value* elem, const RJP_memory_fns* fns);

RJP_index rjp_num_elements(const RJP_value* arr);

/***************** ARRAY ITERATOR OPERATIONS *******************/
void rjp_init_array_iterator(RJP_array_iterator* iter, const RJP_value* array);
void rjp_delete_array_iterator(RJP_array_iterator* iter);
RJP_value* rjp_array_iterator_current(const RJP_array_iterator* it);
RJP_value* rjp_array_iterator_next(RJP_array_iterator* it);
RJP_value* rjp_array_iterator_peek(const RJP_array_iterator* it);


#ifdef __cplusplus
}
#endif
#undef DEPRECATED

#endif

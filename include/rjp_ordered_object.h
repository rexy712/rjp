/**
	rjp
	Copyright (C) 2018-2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RJP_ORDERED_OBJECT_H
#define RJP_ORDERED_OBJECT_H

#include "rjp_internal.h"

struct RJP_ordered_member;

typedef struct RJP_ordered_object{
	struct RJP_ordered_member* members;
	struct RJP_ordered_member* last;
	RJP_index num_members;
}RJP_ordered_object;

void irjp_copy_ordered_object(RJP_value* dest, const RJP_value* src, const RJP_memory_fns* fns);
void irjp_delete_ordered_object(RJP_value* obj, const RJP_memory_fns* fns);

RJP_value* irjp_add_ordered_member(RJP_value* dest, char* key, RJP_index keylen, const RJP_memory_fns* fns);
RJP_value* irjp_remove_ordered_member(RJP_value* obj, RJP_value* member, const RJP_memory_fns* fns);
void irjp_ordered_set_key(RJP_value* dest, char* key, RJP_index keylen, const RJP_memory_fns* fns);
RJP_index irjp_ordered_num_members(const RJP_value* object);
RJP_value* irjp_ordered_search_member(const RJP_value* object, const char* search);
void irjp_init_ordered_object_iterator(RJP_object_iterator* it, const RJP_value* object);
void irjp_delete_ordered_object_iterator(RJP_object_iterator* it);
RJP_value* irjp_ordered_object_iterator_current(const RJP_object_iterator* it);
RJP_value* irjp_ordered_object_iterator_next(RJP_object_iterator* it);
RJP_value* irjp_ordered_object_iterator_peek(const RJP_object_iterator* it);

RJP_index irjp_dump_ordered_object(const RJP_value* root, char* dest);

#endif

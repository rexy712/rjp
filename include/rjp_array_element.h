/**
	rjp
	Copyright (C) 2018-2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE,  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RJP_ARRAY_ELEMENT_H
#define RJP_ARRAY_ELEMENT_H

#include "rjp_internal.h"
#include "rjp_value.h"

typedef struct RJP_array_element{
	struct RJP_value value;
	struct RJP_array_element* next;
	struct RJP_array_element* prev;
}RJP_array_element;

#endif

/**
	rjp
	Copyright (C) 2018-2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE,  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RJP_VALUE_H
#define RJP_VALUE_H

#include "rjp_internal.h"
#include "rjp_object.h"
#include "rjp_array.h"


//Represents json data
//hold any json data typetypetypetype
typedef struct RJP_value{
	union{
		RJP_int integer;
		RJP_float dfloat;
		RJP_bool boolean;
		RJP_ordered_object orobject;
		RJP_unordered_object object;
		RJP_string string;
		RJP_array array;
	};
	struct RJP_value* parent; //pointer to parent (either an array or object or NULL)
	enum RJP_data_type type; //flag to determine active member of union
}RJP_value;

void irjp_delete_value(RJP_value* root, const RJP_memory_fns* fns);

#endif

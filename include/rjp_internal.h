/**
	rjp
	Copyright (C) 2018-2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE,  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RJP_INTERNAL_H
#define RJP_INTERNAL_H

#include "rjp.h"
#include "config.h"
#include <stdio.h>

#ifdef __GNUC__
#define MAYBE_UNUSED __attribute__((unused))
#else
#define MAYBE_UNUSED
#endif

#define UNUSED_VARIABLE(thing) (void)(thing)

#ifdef RJP_ENABLE_DIAGNOSTICS
#define DIAG_PRINT(...) fprintf(__VA_ARGS__)
#else
#define DIAG_PRINT(...) irjp_ignore_unused(__VA_ARGS__)

static inline void irjp_ignore_unused(FILE* fp, ...){
	UNUSED_VARIABLE(fp);
}
#endif

extern const RJP_memory_fns irjp_default_memory_fns;

#endif

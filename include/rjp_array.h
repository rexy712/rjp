/**
	rjp
	Copyright (C) 2018-2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE,  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RJP_ARRAY_H
#define RJP_ARRAY_H

#include "rjp.h"

struct RJP_array_element;

//Represents a json array
typedef struct RJP_array{
	struct RJP_array_element* elements; //linked list of elements
	struct RJP_array_element* last; //final member of linked list
	RJP_index num_elements;
}RJP_array;

void irjp_add_element(RJP_array* j, const RJP_memory_fns* fns);
void irjp_copy_array(RJP_value* dest, const RJP_value* src, const RJP_memory_fns* fns);

RJP_index irjp_dump_array(const RJP_value* arr, char* dest, const int flags, int depth);

#endif

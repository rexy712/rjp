/**
	rjp
	Copyright (C) 2018-2020 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE,  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RJP_STRINGS_H
#define RJP_STRINGS_H

#include "rjp.h"
#include "rjp_internal.h"

int irjp_is_whitespace(char c);
int irjp_copy_string_quoted(char* restrict dest, const char* restrict string, RJP_index length);
int irjp_copy_string_keyed(char* restrict dest, const char* restrict string, RJP_index length);
char* irjp_convert_string(const char* str, RJP_index length, RJP_index* newlen, const RJP_memory_fns* fns);
RJP_index irjp_array_strlen(const RJP_value* arr, const int flags, int depth);
RJP_index irjp_object_strlen(const RJP_value* root, const int flags, int depth);
RJP_index irjp_value_strlen(const RJP_value* root, const int flags, int depth);
void irjp_strcpy(RJP_string* dest, const RJP_string* src, const RJP_memory_fns* fns);

#endif

